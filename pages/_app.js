import 'tailwindcss/tailwind.css'
import Header from '../components/header'
import Footer from './partials/Footer'

import '../styles/login-page.module.css'


function MyApp({ Component, pageProps }) {

  return (
    <div>
      <Header/>
      <Component {...pageProps} />
      <Footer />
    </div>
  )
}

export default MyApp
