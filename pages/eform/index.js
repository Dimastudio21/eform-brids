import React, { useState, useEffect } from 'react'
import Box from '@mui/material/Box';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import BasicInformation from '../../components/basic-information';
import PersonalData from '../../components/personal-data';
import ApplicantsAddress from '../../components/applicants-address';
import Card from '@mui/material/Card';
const steps = ['INFORMASI DASAR', 'DATA PRIBADI PEMOHON', 'ALAMAT PEMOHON','PEKERJAAN DAN REKENING', 'KUESIONER DAN LAMPIRAN'];


export default function Eform(props) {
  const {testProps } = props;
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());
  const [nexts, setNexts ] = useState(false);

  const isStepOptional = (step) => {
    return step === 1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = (e) => {
    setTimeout(() => {
      const next1 = localStorage.getItem("nextStepper1");
      if(next1==="true"){
        setNexts(true);
        onNext();
        localStorage.removeItem('nextStepper1');
      }
      console.log(next1, 'next1');
      
    // }, 5000);
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  
  });
}

  const onNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };
  

  const stepSubmit = activeStep + 1;

  useEffect(()=>{
    console.log(testProps, 'testProps')
    // if(localStorage.getItem('testNext')){
    //   console.log("updates")
    //   onNext();
    //   localStorage.removeItem('testNext')
    // }

  })
  return (
    <>
    <Box sx={{ width: '80%', marginLeft:'10%', marginTop:'5%'  }}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          // if (isStepOptional(index)) {
          //   labelProps.optional = (
          //     <Typography variant="caption">Optional</Typography>
          //   );
          // }
          if (isStepSkipped(index)) {
            stepProps.completed = false;
          }
          return (
            <Step key={label} {...stepProps}>
              <StepLabel {...labelProps}>{label}</StepLabel>
            </Step>
          );
        })}
      </Stepper>
      {activeStep === steps.length ? (
        <React.Fragment>
          <Typography sx={{ mt: 2, mb: 1 }}>
            All steps completed - you&apos;re finished
          </Typography>
          <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
            <Box sx={{ flex: '1 1 auto' }} />
            <Button onClick={handleReset}>Reset</Button>
          </Box>
        </React.Fragment>
      ) : (
        <React.Fragment>
          
          { stepSubmit===1 ? <BasicInformation onSubmit={handleNext}/> : stepSubmit===2 ?<PersonalData/>
           : stepSubmit===3 ? <ApplicantsAddress/> : null}
          <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2}}>
            <Button
              color="inherit"
              disabled={activeStep === 0}
              onClick={handleBack}
              sx={{ mr: 1 }}
              variant="contained"
            >
              Back
            </Button>
            <Box sx={{ flex: '1 1 auto' }} />
            {isStepOptional(activeStep) && (
              <Button color="inherit" onClick={handleSkip} sx={{ mr: 1 }}>
                Skip
              </Button>
            )}
            {
             stepSubmit ===1 ? 
            <Button onClick={onNext} form='my-form' type="submit" variant="contained">
              {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
            </Button> : 
            <Button onClick={onNext} type="submit" variant="contained">
              {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
            </Button>
            }
          </Box>
        </React.Fragment>
      )}      
    </Box>
    <br />
    </>
  );
}

export async function getServerSideProps(context) {
  const user = true
  if (!user) {
    return {
      redirect: {
        destination: '/login',
        permanent: true,
      },
    }
  }

  return {
    props: { user },
  }
}