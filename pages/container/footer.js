import React from 'react'
import Footer from '../../components/footer'
import Icon from '../../components/icons'
import LogoBRIDS from '../../images/logo-danareksa.png'
import Image from 'next/image'


export function FooterContainer() {
    return (
        <Footer>
            <Footer.Wrapper>
            <Footer.Row>
                <Footer.Column>
                <Image src={LogoBRIDS} width="130" />
                <br />
                    <Footer.Link href="#">Gedung BRI II lt 23 Jl. Jend Sudirman Kav. 44-46 Jakarta 10210, Indonesia +62-21-1500-688</Footer.Link>
                    {/* <Footer.Link href="#">callcenter@bridanareksasekuritas.co.id</Footer.Link> */}
                </Footer.Column>
                <Footer.Column>
                <Footer.Title>BUSINESS HOURS</Footer.Title>
                    <Footer.Link href="#">Want to Know more about us ? Give us a call or drops us an email.</Footer.Link>
                    <Footer.Link href="#">08.00 - 17.00 WIB (GMT +7.00)</Footer.Link>
                    <Footer.Link href="#">Senin - Jumat (kecuali hari libur)
                    Monday - Friday (except holidays)</Footer.Link>
                </Footer.Column>
                <Footer.Column>
                <Footer.Title>FOLLOW SOCIAL MEDIA</Footer.Title>
                    <Footer.Link href="#"><Icon className="fab fa-facebook-f" />Facebook</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-instagram" />Instagram</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-youtube" />Youtube</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-twitter" />Twitter</Footer.Link>
                </Footer.Column>
                <Footer.Column>
                <Footer.Title>CONTACT US</Footer.Title>
                    <Footer.Link href="#">Gedung BRI II LT.23 Jl.Jend Sudirman Kav.44-46.
                    Bendungan Hilir, Jakarta Pusat
                    Indonesia</Footer.Link>
                    <Footer.Link href="#">Phone: +62 21-1500-688</Footer.Link>
                    <Footer.Link href="#">Email: callcenter@bridanareksasekuritas.co.id</Footer.Link>
                </Footer.Column>
                {/* <Footer.Column>
                <Footer.Title>Social</Footer.Title>
                    <Footer.Link href="#"><Icon className="fab fa-facebook-f" />Facebook</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-instagram" />Instagram</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-youtube" />Youtube</Footer.Link>
                    <Footer.Link href="#"><Icon className="fab fa-twitter" />Twitter</Footer.Link>
                </Footer.Column> */}
            </Footer.Row>
            </Footer.Wrapper>
        </Footer>
    )
}