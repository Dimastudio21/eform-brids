import {withIronSession} from "next-iron-session";
import {default as sessionConfig} from "../../config/session"
import {auth, user} from "../../lib/service";

export default withIronSession(
  async (req, res) => {

    if (req.method === "POST") {

      const {username, password} = req.body;
      let authResponse = await auth.login(username, password);

      if (!authResponse.error) {

        let userResponse = await user.get(authResponse.id, authResponse.credential.token)
        req.session.set("user", userResponse);
        await req.session.save();
        return res.status(200).send(userResponse);

      } else {

        return res.status(500).send("Something went wrong, Err X001!");

      }

    }

    return res.status(404).send("");
  },
  sessionConfig
);