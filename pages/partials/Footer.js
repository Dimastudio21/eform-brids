import React from 'react';
import { Link } from 'react-router-dom';
import Logodanareksa from '../../images/logo-danareksa.png'
import Image from 'next/image'
import Ojk from '../../images/ojk.png'
import Akses from '../../images/akses.png'
import Idclear from '../../images/idclear.png'
import Idx from '../../images/idx.png'
import Nabungsaham from '../../images/nabung-saham.png'
import Keuangan from '../../images/keuangan.png'



function Footer() {
  return (
 <footer>
        <div style={{backgroundColor: '#040919'}}>
        <div className="xl:px-30 pb-9 lg:px-18 md:px-10 sm:px-9 px-19">
          <div className="w-full pt-12 flex flex-col sm:flex-row space-y-2 justify-start">
            <div className="w-full sm:w-2/5 pr-6 flex flex-col space-y-4">
            <Image src={Logodanareksa} width={490} height={100}/>
              <p className=" text-white">Gedung BRI II lt 23 Jl. Jend Sudirman Kav. 44-46 Jakarta 10210, Indonesia +62-21-1500-688 callcenter@bridanareksasekuritas.co.id</p>
            </div>
              <div className="w-full sm:w-2/4 flex flex-col space-y-4">
              <a className="text-white">BUSINESS HOURS</a>
              <a className="text-white">Want to know more about us? Give us a call or drop us an email.</a>
              <a className="text-white">08.00 - 17.00 WIB (GMT +7.00)</a>
              <a className="text-white">Senin - Jumat (kecuali hari libur)
              Monday - Friday (except holidays)</a>
            </div>
            <div className="w-full sm:w-1/4 flex flex-col space-y-4">
              <a className="text-white">FOLLOW SOCIAL MEDIA</a>
              <a className="text-white">TWITTER</a>
              <a className="text-white">FACEBOOK</a>
              <a className="text-white">INSTAGRAM</a>
              <a className="text-white">GOOGLE</a>
              <a className="text-white">LINKEDIN</a>
            </div>
            <div className="w-full sm:w-2/4 flex flex-col space-y-4">
              <a className="text-white">CONTACT US</a>
              <a className="text-white">Gedung BRI II LT.23 Jl.Jend Sudirman Kav.44-46.
              Bendungan Hilir, Jakarta Pusat
              Indonesia.</a>
              <a className="text-white">Phone: +62 21-1500-688)</a>
              <a className="text-white">Email: callcenter@bridanareksasekuritas.co.id</a>
            </div>
            <div className="w-full sm:w-1/5 pt-6 flex items-end mb-1">
              <div className="flex flex-row space-x-4">
                <i className="fab fa-facebook-f"></i>
                <i className="fab fa-twitter"></i>
                <i className="fab fa-instagram"></i>
                <i className="fab fa-google"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-col">
               
                <hr style={{backgroundColor: '#040919'}}/>
                <p className="w-full text-center my-4 text-gray-600">Copyright © 2021 BRI DANAREKSA SEKURTIAS</p>
            </div>
 </footer>
  );
}

export default Footer;
