import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import HeroHome from '../pages/partials/HeroHome'
import Testimonials from './partials/Testimonials'
import WorkFlow from './partials/workflow'
import {ThemeProvider} from 'theme-ui'
import Newsletter from '../pages/partials/Newsletter'
// import Header from '../components/header'

export default function Home() {
  return (
  <div className="flex flex-col min-h-screen overflow-hidden">
      <HeroHome />
      <Testimonials />
      {/* <WorkFlow /> */}
      <Newsletter />
      {/* <FooterContainer /> */}
    </div>
  )
}
