import React from "react";
import { useForm } from "react-hook-form";
import { useState } from 'react';
import Link from 'next/link';

export default function BasicInformation(props) {
//   const { onSubmit } = props;
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [ showReguler, setShowReguler ] = useState(false)
  const [ showSyariah, setShowSyariah ] = useState(false)
  const [ nasabahBri, setNasabahBri ] = useState(false)
  const [ showOtp, setShowOtp ] = useState(false)
  const [ showNotBri, setShowNotBri ] = useState(false)

  const onSubmit = data => submitData(data);
  
  const handleChangeTypeRek = (e) => {
      console.log(e.target.value)
        if(e.target.value === 'Reguler'){
            setShowReguler(true);
            setShowSyariah(false);
        } else if(e.target.value === 'Syariah') {
            setShowReguler(false);
            setShowSyariah(true);
            setNasabahBri(false);
            setShowOtp(false);
            setShowNotBri(false)
        } else {
            setShowReguler(false);
            setShowSyariah(false);
        }
  }

  const handleChangeReferensi = e => {
    if(e.target.value === 'ya'){
        setNasabahBri(true);
    } else {
        setNasabahBri(false);
        setShowOtp(false);
        setShowNotBri(false)
    }
  }

  const handleChangeShowOtp = e => {
    if(e.target.value === 'ya'){
        setShowOtp(true);
        setShowNotBri(false)
    } else {
        setShowOtp(false);
        setShowNotBri(true);
    } 
  }

  const submitData = (data) => {
    console.log(data, 'datas')
    localStorage.setItem('nextStepper1', 'true')
  }

  
  return (
    <div className="border-solid mx-auto bg-white overflow-hidden border-2 mt-20 w-3/5">
                            <div className="md:flex bg-blue-450 m-8 rounded-2xl">
                                <div style={styles.card}>
                                    <p style={styles.title}>INFORMASI PRIBADI</p>
                                </div>
                            </div>

                            <form id='my-form' className="ml-8 mr-8"
                                onSubmit={handleSubmit(onSubmit)}>
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                           Branch (SID) <span style={{color:"red"}}>*</span>
                                        </label>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                    <div className="relative">
                                        <select {...register("branchId",{required: true})} name="branchId" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                                        >
                                          <option value=""></option>
                                          <option value="B0394">B0394</option>
                                          <option value="B0396">B0396</option>
                                        </select>
                                        {errors.branchId && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                                        </div>                                    
                                    </div>
                                </div>
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                           Referal Code
                                        </label>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <input 
                                        className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700" 
                                        name="referalCode"

                                        autoFocus 
                                        {...register("referalCode")} /> 
                                        {errors.referalCode && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                                    </div>
                                </div>
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                           Jenis rekening yang Anda inginkan ? <span style={{color:"red"}}>*</span>
                                        </label>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                    <div className="relative">
                                        <select {...register("typeRek",{required: true})} name="typeRek" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                                        onChange={handleChangeTypeRek}>
                                          <option value=""></option>
                                          <option value="Reguler">Reguler</option>
                                          <option value="Syariah">Syariah</option>
                                        </select>
                                        {errors.typeRek && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                                        </div>                                    
                                    </div>
                                </div>
                                {showReguler ? 
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Apakah anda mendapat referensi dari BRI ? <span style={{color:"red"}}>*</span>
                                        </label>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                    <div className="relative">
                                        <select {...register("isReferensiBri",{required: true})} name="isReferensiBri" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                                        onChange={handleChangeReferensi}>
                                        <option value=""></option>
                                        <option value="ya">YA</option>
                                        <option value="tidak">TIDAK</option>
                                        </select>
                                        {errors.isReferensiBri && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                                    </div>                                    
                                    </div>
                                </div> : null}
                                {nasabahBri ? 
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Apakah saat ini Anda sebagai nasabah BRI? <span style={{color:"red"}}>*</span>
                                        </label>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                    <div className="relative">
                                        <select {...register("isNasabahBri",{required: true})} name="isNasabahBri" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                                        onChange={handleChangeShowOtp}>
                                        <option value=""></option>
                                        <option value="ya">YA</option>
                                        <option value="tidak">TIDAK</option>
                                        </select>
                                        {errors.isNasabahBri && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}

                                    </div>                                   
                                    </div>
                                </div> : null}
                                
                                <div className="flex flex-wrap -mx-3 mb-2">
                                {showSyariah ? 
                                    <div className="md:flex rounded-2xl items-center px-16">
                                        <div style={styles.card}>
                                        <p style={{textAlign:"center"}} className="block tracking-wide text-gray-700 text-xs font-bold">
                                            DSN-MUI No. 80/DSN-MUI/III/2011 - Penerapan Prinsip Syariah Perdagangan Efek Ekuitas, silahkan klik <Link href='/auth/login'><span style={{color:"blue"}}>disini</span></Link> dan fatwa <Link href='/auth/login'><span style={{color:"blue"}}>lainnya.</span></Link>
                                            </p>
                                        </div>
                                    </div> : null}
                                {showNotBri ? 
                                    <div className="md:flex rounded-2xl items-center px-16">
                                        <div style={styles.card}>
                                        <p style={{textAlign:"center"}} 
                                        className="block tracking-wide text-gray-700 text-xs font-bold">
                                            Untuk melanjutkan proses pembukuaan rekening BRI Danareksa, 
                                            kami sarankan Anda untuk membuka rekening Bank BRI melalui link berikut ini : 
                                            <Link href="/auth/login"><span style={{color:"blue"}}>bukarekening.bri.co.id</span>
                                            </Link></p>
                                        </div>
                                    </div> : null}  
                                </div>
                                <div className="flex flex-wrap -mx-3 mb-2">
                                {showOtp ? 
                                    <div className="md:flex rounded-2xl items-center px-16">
                                        <div style={styles.card}>
                                        <p style={{textAlign:"center"}} className="block tracking-wide text-gray-700 text-xs font-bold">Pastikan Nomor Handphone dan Nomor Rekening Anda sudah terdaftar di BRI.
                                        Nomor Hanphone dan Nomor Rekening BRI akan kami kirimkan ke BRI untuk proses verifikasi.</p></div>
                                    </div> : null} 
                                </div>
                                {showOtp ? 
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        No Handphone
                                        </label>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                    <input 
                                        className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700" 
                                        name="noHp" 
                                        autoFocus 
                                        {...register("noHp",{required: true, pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/})} /> 
                                        {console.log(errors.noHp)}
                                        {errors.noHp && errors.noHp.type==="required" && (<div className="mb-3 text-normal text-red-500">{"Field is required"}</div>)}
                                        {errors.noHp && errors.noHp.type==="pattern" && (<div className="mb-3 text-normal text-red-500">{"Field is Number"}</div>)}                                  
                                    </div>
                                </div> : null}
                                {showOtp ? 
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        No Rekening BRI
                                        </label>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3 w-2/4">
                                    <input 
                                        className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700" 
                                        name="NoRekBri" 
                                        autoFocus 
                                        {...register("NoRekBri",{required: true, pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/})} /> 
                                        {errors.NoRekBri && errors.NoRekBri.type==="required" && (<div className="mb-3 text-normal text-red-500">{"Field is required"}</div>)} 
                                        {errors.NoRekBri && errors.NoRekBri.type==="pattern" && (<div className="mb-3 text-normal text-red-500 ">{"Field is Number"}</div>)}                                  
                                    </div>
                                </div> : null}
                                {showOtp ? 
                                     <div className="flex justify-center mb-3">
                                     <button className="bg-yellow-500 text-white font-bold py-2 px-4 rounded"
                                      type="submit" onClick>
                                         Kirim OTP
                                     </button>
                                     </div> : null}
                            </form>
                        </div>
  );
}

const styles = {
  container: {
      minHeight: 'inherit',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
  },
  card: {
      margin: 25
  },
  title: {
      color: "#FFFFFF"
  }, banner: {
      pt: ['140px', '145px', '155px', '170px', null, null, '180px', '215px'],
      pb: [2, null, 0, null, 2, 0, null, 5],
      position: 'relative',
      zIndex: 2,
      '&::before': {
          position: 'absolute',
          content: '""',
          bottom: 6,
          left: 0,
          height: '100%',
          width: '100%',
          zIndex: -1,
          backgroundRepeat: `no-repeat`,
          backgroundPosition: 'bottom left',
          backgroundSize: '36%',
      },
      '&::after': {
          position: 'absolute',
          content: '""',
          bottom: '40px',
          right: 0,
          height: '100%',
          width: '100%',
          zIndex: -1,
          backgroundRepeat: `no-repeat`,
          backgroundPosition: 'bottom right',
          backgroundSize: '32%',
      },
      container: {
          minHeight: 'inherit',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
      },
      contentBox: {
          width: ['100%', '90%', '535px', null, '57%', '60%', '68%', '60%'],
          mx: 'auto',
          textAlign: 'center',
          mb: ['40px', null, null, null, null, 7],
      },
      imageBox: {
          justifyContent: 'center',
          textAlign: 'center',
          display: 'inline-flex',
          mb: [0, null, -6, null, null, '10px', null, -3],
          img: {
              position: 'relative',
              height: [1000],
          },
      },
  },
}