import { useState } from 'react';


export default function Informasi() {

    const [getImgaeLeft, setImageLeft] = useState(null);
    const [getImgaeRight, setImageRight] = useState(null);
    const [isDeletePhotoLeft, setIsDeletePhotoLeft] = useState(false);
    const [isDeletePhotoRight, setIsDeleteRight ] = useState(false);



    const handleChangeImageLeft = (e) => {
        if (URL.createObjectURL(e.target.files[0]) !== null) {
            let imageLeft = URL.createObjectURL(e.target.files[0]);
            setImageLeft(imageLeft);
            setIsDeletePhotoLeft(true);
        }
    }

    const handleChangeImageRight = (e) => {
        if (URL.createObjectURL(e.target.files[0]) !== null) {
            let imageRight = URL.createObjectURL(e.target.files[0]);
            setImageRight(imageRight);
            setIsDeleteRight(true);
        }
    }

    const deletePhotoLeft = () => {
          setImageLeft(null);
          setIsDeletePhotoLeft(false);
    }

    const deletePhotoRight = () => {
        setImageRight(null);
        setisDeletePhotoRight(false);
  }

    return (

                        <div className="border-solid  mx-auto bg-white overflow-hidden border-2 mt-40 mr-10 ml-10 shadow-2xl">
                            <div className="md:flex bg-blue-450 m-8 rounded-2xl">
                                <div style={styles.card}>
                                    <p style={styles.title}>ALAMAT PEMOHON</p>
                                </div>
                            </div>

                            <form className="ml-8 mr-8">
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    {/* <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                                            Branch ID
                                        </label>
                                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text"/>
                                        <p className="text-red-500 text-xs italic">Please fill out this field.</p>
                                    </div> */}
                                    <div className="w-full md:w-1/2 px-3">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                                           Alamat
                                        </label>
                                        <input className="" id="grid-last-name" type="text"/>
                                    </div>
                                    <div className="w-full md:w-1/2 px-3">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                                            Referal Code
                                        </label>
                                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-last-name" type="text"/>
                                    </div>
                                </div>
                                <div className="flex flex-wrap -mx-3 mb-2">
                                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                                        Jenis rekening yang anda inginkan ?
                                        </label>
                                        <div className="relative">
                                            <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                                                <option>Regular</option>
                                                <option>Syariah</option>
                                            </select>
                                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-city">
                                            Apakah anda mendapatkan referensi dari BRI
                                        </label>
                                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-city" type="text"/>
                                    </div>
                                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                                        Apakah anda saat ini sebagai nasabah BRI ?
                                        </label>
                                        <div className="relative">
                                            <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
                                                <option>YA</option>
                                                <option>TIDAK</option>
                                            </select>
                                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3">
                                        <div className="md:flex border-solid border-2 bg-gray-200 h-52">
                                            <div style={styles.card}>
                                                <img src={getImgaeLeft} />
                                            </div>
                                        </div>
                                        {/* <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                                            Referal Code
                                        </label>
                                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                            id="grid-last-name"
                                            type="file"
                                            accept="image/*"
                                            onChange={handleChangeImageLeft} /> */}
                                        <label
                                        className="md:flex bg-blue-451 rounded-md shadow-md tracking-wide border border-blue cursor-pointer hover:bg-blue-700 hover:text-white text-white ease-linear transition-all duration-150">
                                        <i className="fas fa-cloud-upload-alt fa-3x"></i>
                                        <span className="m-2 text-base content-center">{ isDeletePhotoLeft ? "Delete Photo" : "Unggah swafoto dengan eKTP"}</span>
                                        
                                        {
                                         isDeletePhotoLeft  ?   
                                         <button  onChange={deletePhotoLeft}/> :
                                         <input type='file' 
                                          class="hidden"
                                          type="file"
                                          accept="image/*"
                                          onChange={isDeletePhotoLeft ? deletePhotoLeft : handleChangeImageLeft} />
                                        }
                                        </label>
                                        
                                    </div>
                                    <div className="w-full md:w-1/2 px-3">
                                        <div className="md:flex border-solid border-2 bg-gray-200 h-52">
                                            <div style={styles.card}>
                                                <img src={getImgaeRight} />
                                            </div>
                                        </div>
                                        <label
                                        className="md:flex bg-blue-451 rounded-md shadow-md tracking-wide border border-blue cursor-pointer hover:bg-blue-700 hover:text-white text-white ease-linear transition-all duration-150">
                                        <i className="fas fa-cloud-upload-alt fa-3x"></i>
                                        <span className="m-2 text-base content-center">{ isDeletePhotoRight ? "Delete Photo" : "Unggah swafoto dengan eKTP"}</span>
                                        
                                        {
                                         isDeletePhotoRight  ?   
                                         <button  onChange={deletePhotoRight}/> :
                                         <input type='file' 
                                          class="hidden"
                                          type="file"
                                          accept="image/*"
                                          onChange={handleChangeImageRight} />
                                        }
                                        </label>
                                        <div className="flex justify-end mt-3">
                                        <button class="bg-blue-451 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                                            Lanjut
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    
    )
}

const styles = {
    container: {
        minHeight: 'inherit',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    card: {
        margin: 25
    },
    title: {
        color: "#FFFFFF"
    }, banner: {
        pt: ['140px', '145px', '155px', '170px', null, null, '180px', '215px'],
        pb: [2, null, 0, null, 2, 0, null, 5],
        position: 'relative',
        zIndex: 2,
        '&::before': {
            position: 'absolute',
            content: '""',
            bottom: 6,
            left: 0,
            height: '100%',
            width: '100%',
            zIndex: -1,
            backgroundRepeat: `no-repeat`,
            backgroundPosition: 'bottom left',
            backgroundSize: '36%',
        },
        '&::after': {
            position: 'absolute',
            content: '""',
            bottom: '40px',
            right: 0,
            height: '100%',
            width: '100%',
            zIndex: -1,
            backgroundRepeat: `no-repeat`,
            backgroundPosition: 'bottom right',
            backgroundSize: '32%',
        },
        container: {
            minHeight: 'inherit',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
        },
        contentBox: {
            width: ['100%', '90%', '535px', null, '57%', '60%', '68%', '60%'],
            mx: 'auto',
            textAlign: 'center',
            mb: ['40px', null, null, null, null, 7],
        },
        imageBox: {
            justifyContent: 'center',
            textAlign: 'center',
            display: 'inline-flex',
            mb: [0, null, -6, null, null, '10px', null, -3],
            img: {
                position: 'relative',
                height: [1000],
            },
        },
    },
}