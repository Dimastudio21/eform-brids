import React from "react";
import { useForm } from "react-hook-form";
import { useState } from 'react';
import IconImage from '../../assets/image/icon-delete.png';
import Image from 'next/image'
export default function PersonalData() {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [getPhotoKTP, setPhotoKTP] = useState(null);
    const [isDeletePhotoKTP, setIsDeleteKTP] = useState(false);

    const handleChangeImageKTP = (e) => {
        if (URL.createObjectURL(e.target.files[0]) !== null) {
            let photoKTP = URL.createObjectURL(e.target.files[0]);
            setPhotoKTP(photoKTP);
            setIsDeleteKTP(true);
        }
    }

    const deletePhotoKTP = () => {
        setPhotoKTP(null);
        setIsDeleteKTP(false);
    }
    const onSubmit = data => console.log(data);

    return (
        <div className="border-solid  mx-auto bg-white overflow-hidden border-2 mt-10 mr-10 ml-10">
            <div className="md:flex bg-blue-450 m-8 rounded-2xl">
                <div style={{margin:25}}>
                    <p style={{color: "#FFFFFF"}}>DATA PRIBADI</p>
                </div>
            </div>
            <form className="ml-8 mr-8"
                onSubmit={handleSubmit(onSubmit)}>
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 border-gray-500 text-xs font-bold mb-2" >
                            Nomor Ponsel <span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                            className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            name="nomorHp"
                            autoFocus {...register("nomorHp", { required: true })} />
                        {errors.nomorHp && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" >
                            Email <span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                            className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                            name="email"
                            autoFocus {...register("email", { required: true })} />
                        {errors.email && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                    </div>
                </div>
                {/* border */}
                <div className="flex flex-wrap -mx-3 mb-6 border-b-2" />
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" >
                            Nama Sesuai Identitas <span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                            className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            name="name"
                            autoFocus {...register("name", { required: true })} />
                        {errors.name && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}

                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" >
                            Tempat Lahir <span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                            className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            name="placeOfBirth"
                            autoFocus {...register("placeOfBirth", { required: true })} />
                        {errors.placeOfBirth && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}

                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Tanggal Lahir <span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                            className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            name="dateOfBirth"
                            autoFocus {...register("dateOfBirth", { required: true })} />
                        {errors.dateOfBirth && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" >
                            No KTP <span style={{ color: "red" }}>*</span>
                        </label>
                        <div className="flex items-center justify-center w-full">
                            <label
                                className="flex flex-col w-full h-44 border-4 hover:border-blue-200 border-dashed bg-gray-100 border-gray-300">
                                <div className="flex flex-col items-center justify-center pt-7">

                                    {isDeletePhotoKTP ?
                                        <div>
                                            <button onClick={deletePhotoKTP} type="button">
                                                {/* <Image src={IconImage} alt="Icon Delete" /> */}
                                            </button>
                                            <Image src={getPhotoKTP} width={200} height={130} alt="Image KTP" />
                                        </div>
                                        :
                                        <div className="flex flex-col items-center justify-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="w-8 h-8 text-gray-400 group-hover:text-gray-600"
                                                fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                    d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
                                            </svg>
                                            <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">Attach a file</p>
                                            <input type='file'
                                                className="hidden"
                                                type="file"
                                                accept="image/*"
                                                onChange={handleChangeImageKTP} />
                                        </div>
                                    }
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" >
                            Agama <span style={{ color: "red" }}>*</span>
                        </label>
                        <div className="relative">
                            <select {...register("religion", { required: true })} name="religion" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            >
                                <option value=""></option>
                                <option value="isalam">ISLAM</option>
                                <option value="keristen">KERISTEN</option>
                                <option value="katolik">KATOLIK</option>
                                <option value="budha">BUDHA</option>
                                <option value="hindu">HINDU</option>
                            </select>
                            {errors.religion && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                        </div>                                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mt-2 mb-2" >
                            Jenis Kelamin <span style={{ color: "red" }}>*</span>
                        </label>
                        <input {...register("genderM", { required: true })} type="radio" value="male" /><span className="ml-2 mr-2 text-gray-700">Laki-Laki</span>
                        <input {...register("genderF", { required: true })} type="radio" value="female" /><span className="ml-2 text-gray-700">Perempuan</span>
                    </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" >
                            Nama Ibu Kandung <span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                            className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            name="yourMotherName"
                            autoFocus {...register("yourMotherName", { required: true })} />
                        {errors.yourMotherName && (<div className="mb-3 text-normal text-red-500 ">{"Harus di isi dong"}</div>)}                                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Status Pernikahan <span style={{ color: "red" }}>*</span>
                        </label>
                        <div className="relative">
                            <select {...register("meriedStatus", { required: true })} name="meriedStatus" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            >
                                <option value=""></option>
                                <option value="kawin">KAWIN</option>
                                <option value="tidak">BELUM KAWIN</option>
                            </select>
                            {errors.meriedStatus && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                        </div>
                    </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Nama Pewaris <span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                            className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            name="pewarisName"
                            autoFocus {...register("pewarisName", { required: true })} />
                        {errors.pewarisName && (<div className="mb-3 text-normal text-red-500 ">{"Harus di isi dong"}</div>)}                                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Hubungan Dengan Pewaris <span style={{ color: "red" }}>*</span>
                        </label>
                        <div className="relative">
                            <select {...register("pewarisRelation", { required: true })} name="pewarisRelation" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            >
                                <option value=""></option>
                                <option value="saudara">SAUDARA</option>
                                <option value="lainnya">LAINNYA</option>
                            </select>
                            {errors.pewarisRelation && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                        </div>
                    </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Nomor NPWP
                        </label>
                        <input
                            className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            name="NoNpwp"
                            autoFocus {...register("NoNpwp", { required: true })} />
                        {errors.NoNpwp && (<div className="mb-3 text-normal text-red-500 ">{"Harus di isi dong"}</div>)}                                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Pendidikan <span style={{ color: "red" }}>*</span>
                        </label>
                        <div className="relative">
                            <select {...register("education", { required: true })} name="education" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            >
                                <option value=""></option>
                                <option value="sd">SD</option>
                                <option value="smp">SMP</option>
                                <option value="SMA">SMA</option>
                                <option value="S1">SARJANA</option>
                            </select>
                            {errors.education && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                        </div>
                    </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6">
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Alasan Tidak Punya NPWP
                        </label>
                        <input
                            className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            name="reasonNotNPWP"
                            autoFocus {...register("reasonNotNPWP", { required: true })} />
                        {errors.reasonNotNPWP && (<div className="mb-3 text-normal text-red-500 ">{"Harus di isi dong"}</div>)}                                    </div>
                    <div className="w-full md:w-1/2 px-3">
                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2">
                            Alamat Korespondensi <span style={{ color: "red" }}>*</span>
                        </label>
                        <div className="relative">
                            <select {...register("korespondensiAddress", { required: true })} name="korespondensiAddress" className="border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700"
                            >
                                <option value="">--PILIH SATU--</option>
                                <option value="ALAMAT">CONTOH</option>
                            </select>
                            {errors.korespondensiAddress && (<div className="mb-3 text-normal text-red-500 ">{"Field is required"}</div>)}
                        </div>
                    </div>
                </div>
                <div className="flex flex-wrap -mx-3 mb-6 border-b-2" />
            </form>
        </div>
    );
}