"use strict";
self["webpackHotUpdate_N_E"]("pages/_app",{

/***/ "./pages/partials/Footer.js":
/*!**********************************!*\
  !*** ./pages/partials/Footer.js ***!
  \**********************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _images_logo_danareksa_png__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../images/logo-danareksa.png */ "./images/logo-danareksa.png");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/image */ "./node_modules/next/image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _images_ojk_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../images/ojk.png */ "./images/ojk.png");
/* harmony import */ var _images_akses_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../images/akses.png */ "./images/akses.png");
/* harmony import */ var _images_idclear_png__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../images/idclear.png */ "./images/idclear.png");
/* harmony import */ var _images_idx_png__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../images/idx.png */ "./images/idx.png");
/* harmony import */ var _images_nabung_saham_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../images/nabung-saham.png */ "./images/nabung-saham.png");
/* harmony import */ var _images_keuangan_png__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../images/keuangan.png */ "./images/keuangan.png");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__);
/* module decorator */ module = __webpack_require__.hmd(module);
var _jsxFileName = "C:\\NEXTJS\\eform-bri\\pages\\partials\\Footer.js";












function Footer() {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("footer", {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
      style: {
        backgroundColor: '#040919'
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
        className: "xl:px-30 pb-9 lg:px-18 md:px-10 sm:px-9 px-19",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
          className: "w-full pt-12 flex flex-col sm:flex-row space-y-2 justify-start",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
            className: "w-full sm:w-2/5 pr-6 flex flex-col space-y-4",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_2___default()), {
              src: _images_logo_danareksa_png__WEBPACK_IMPORTED_MODULE_1__.default,
              width: 490,
              height: 100
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 21,
              columnNumber: 13
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("p", {
              className: " text-white",
              children: "Gedung BRI II lt 23 Jl. Jend Sudirman Kav. 44-46 Jakarta 10210, Indonesia +62-21-1500-688 callcenter@bridanareksasekuritas.co.id"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 22,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 20,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
            className: "w-full sm:w-2/4 flex flex-col space-y-4",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "BUSINESS HOURS"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 25,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "Want to know more about us? Give us a call or drop us an email."
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 26,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "08.00 - 17.00 WIB (GMT +7.00)"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 27,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "Senin - Jumat (kecuali hari libur) Monday - Friday (except holidays)"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 28,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 24,
            columnNumber: 15
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
            className: "w-full sm:w-1/4 flex flex-col space-y-4",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "FOLLOW SOCIAL MEDIA"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 32,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "TWITTER"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 33,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "FACEBOOK"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "INSTAGRAM"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 35,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "GOOGLE"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 36,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "LINKEDIN"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 37,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 31,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
            className: "w-full sm:w-2/4 flex flex-col space-y-4",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "CONTACT US"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 40,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "Gedung BRI II LT.23 Jl.Jend Sudirman Kav.44-46. Bendungan Hilir, Jakarta Pusat Indonesia."
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 41,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "Phone: +62 21-1500-688)"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 44,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
              className: "text-white",
              children: "Email: callcenter@bridanareksasekuritas.co.id"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 45,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
            className: "w-full sm:w-1/5 pt-6 flex items-end mb-1",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
              className: "flex flex-row space-x-4",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("i", {
                className: "fab fa-facebook-f"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 49,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("i", {
                className: "fab fa-twitter"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 50,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("i", {
                className: "fab fa-instagram"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 51,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("i", {
                className: "fab fa-google"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 52,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 48,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 47,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 19,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 9
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
      className: "flex flex-col",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("hr", {
        style: {
          backgroundColor: '#040919'
        }
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 17
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("p", {
        className: "w-full text-center my-9 text-gray-600",
        children: "Copyright \xA9 2021 BRI DANAREKSA SEKURTIAS"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 61,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 2
  }, this);
}

_c = Footer;
/* harmony default export */ __webpack_exports__["default"] = (Footer);

var _c;

$RefreshReg$(_c, "Footer");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvX2FwcC43Mjk0MmZlMzQ4NjVkY2QzMzY1OC5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBSUEsU0FBU1UsTUFBVCxHQUFrQjtBQUNoQixzQkFDRDtBQUFBLDRCQUNPO0FBQUssV0FBSyxFQUFFO0FBQUNDLFFBQUFBLGVBQWUsRUFBRTtBQUFsQixPQUFaO0FBQUEsNkJBQ0E7QUFBSyxpQkFBUyxFQUFDLCtDQUFmO0FBQUEsK0JBQ0U7QUFBSyxtQkFBUyxFQUFDLGdFQUFmO0FBQUEsa0NBQ0U7QUFBSyxxQkFBUyxFQUFDLDhDQUFmO0FBQUEsb0NBQ0EsOERBQUMsbURBQUQ7QUFBTyxpQkFBRyxFQUFFVCwrREFBWjtBQUEyQixtQkFBSyxFQUFFLEdBQWxDO0FBQXVDLG9CQUFNLEVBQUU7QUFBL0M7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFEQSxlQUVFO0FBQUcsdUJBQVMsRUFBQyxhQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQUtJO0FBQUsscUJBQVMsRUFBQyx5Q0FBZjtBQUFBLG9DQUNBO0FBQUcsdUJBQVMsRUFBQyxZQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURBLGVBRUE7QUFBRyx1QkFBUyxFQUFDLFlBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRkEsZUFHQTtBQUFHLHVCQUFTLEVBQUMsWUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFIQSxlQUlBO0FBQUcsdUJBQVMsRUFBQyxZQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFMSixlQVlFO0FBQUsscUJBQVMsRUFBQyx5Q0FBZjtBQUFBLG9DQUNFO0FBQUcsdUJBQVMsRUFBQyxZQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGLGVBRUU7QUFBRyx1QkFBUyxFQUFDLFlBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRkYsZUFHRTtBQUFHLHVCQUFTLEVBQUMsWUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFIRixlQUlFO0FBQUcsdUJBQVMsRUFBQyxZQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUpGLGVBS0U7QUFBRyx1QkFBUyxFQUFDLFlBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBTEYsZUFNRTtBQUFHLHVCQUFTLEVBQUMsWUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFORjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBWkYsZUFvQkU7QUFBSyxxQkFBUyxFQUFDLHlDQUFmO0FBQUEsb0NBQ0U7QUFBRyx1QkFBUyxFQUFDLFlBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFFRTtBQUFHLHVCQUFTLEVBQUMsWUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFGRixlQUtFO0FBQUcsdUJBQVMsRUFBQyxZQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUxGLGVBTUU7QUFBRyx1QkFBUyxFQUFDLFlBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBTkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQXBCRixlQTRCRTtBQUFLLHFCQUFTLEVBQUMsMENBQWY7QUFBQSxtQ0FDRTtBQUFLLHVCQUFTLEVBQUMseUJBQWY7QUFBQSxzQ0FDRTtBQUFHLHlCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURGLGVBRUU7QUFBRyx5QkFBUyxFQUFDO0FBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFGRixlQUdFO0FBQUcseUJBQVMsRUFBQztBQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBSEYsZUFJRTtBQUFHLHlCQUFTLEVBQUM7QUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBNUJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBRFAsZUEwQ0s7QUFBSyxlQUFTLEVBQUMsZUFBZjtBQUFBLDhCQUVVO0FBQUksYUFBSyxFQUFFO0FBQUNTLFVBQUFBLGVBQWUsRUFBRTtBQUFsQjtBQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGVixlQUdVO0FBQUcsaUJBQVMsRUFBQyx1Q0FBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUhWO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQTFDTDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFEQztBQWtERDs7S0FuRFFEO0FBcURULCtEQUFlQSxNQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vX05fRS8uL3BhZ2VzL3BhcnRpYWxzL0Zvb3Rlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgeyBMaW5rIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSc7XHJcbmltcG9ydCBMb2dvZGFuYXJla3NhIGZyb20gJy4uLy4uL2ltYWdlcy9sb2dvLWRhbmFyZWtzYS5wbmcnXHJcbmltcG9ydCBJbWFnZSBmcm9tICduZXh0L2ltYWdlJ1xyXG5pbXBvcnQgT2prIGZyb20gJy4uLy4uL2ltYWdlcy9vamsucG5nJ1xyXG5pbXBvcnQgQWtzZXMgZnJvbSAnLi4vLi4vaW1hZ2VzL2Frc2VzLnBuZydcclxuaW1wb3J0IElkY2xlYXIgZnJvbSAnLi4vLi4vaW1hZ2VzL2lkY2xlYXIucG5nJ1xyXG5pbXBvcnQgSWR4IGZyb20gJy4uLy4uL2ltYWdlcy9pZHgucG5nJ1xyXG5pbXBvcnQgTmFidW5nc2FoYW0gZnJvbSAnLi4vLi4vaW1hZ2VzL25hYnVuZy1zYWhhbS5wbmcnXHJcbmltcG9ydCBLZXVhbmdhbiBmcm9tICcuLi8uLi9pbWFnZXMva2V1YW5nYW4ucG5nJ1xyXG5cclxuXHJcblxyXG5mdW5jdGlvbiBGb290ZXIoKSB7XHJcbiAgcmV0dXJuIChcclxuIDxmb290ZXI+XHJcbiAgICAgICAgPGRpdiBzdHlsZT17e2JhY2tncm91bmRDb2xvcjogJyMwNDA5MTknfX0+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ4bDpweC0zMCBwYi05IGxnOnB4LTE4IG1kOnB4LTEwIHNtOnB4LTkgcHgtMTlcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIHB0LTEyIGZsZXggZmxleC1jb2wgc206ZmxleC1yb3cgc3BhY2UteS0yIGp1c3RpZnktc3RhcnRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LWZ1bGwgc206dy0yLzUgcHItNiBmbGV4IGZsZXgtY29sIHNwYWNlLXktNFwiPlxyXG4gICAgICAgICAgICA8SW1hZ2Ugc3JjPXtMb2dvZGFuYXJla3NhfSB3aWR0aD17NDkwfSBoZWlnaHQ9ezEwMH0vPlxyXG4gICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIiB0ZXh0LXdoaXRlXCI+R2VkdW5nIEJSSSBJSSBsdCAyMyBKbC4gSmVuZCBTdWRpcm1hbiBLYXYuIDQ0LTQ2IEpha2FydGEgMTAyMTAsIEluZG9uZXNpYSArNjItMjEtMTUwMC02ODggY2FsbGNlbnRlckBicmlkYW5hcmVrc2FzZWt1cml0YXMuY28uaWQ8L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIHNtOnctMi80IGZsZXggZmxleC1jb2wgc3BhY2UteS00XCI+XHJcbiAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwidGV4dC13aGl0ZVwiPkJVU0lORVNTIEhPVVJTPC9hPlxyXG4gICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cInRleHQtd2hpdGVcIj5XYW50IHRvIGtub3cgbW9yZSBhYm91dCB1cz8gR2l2ZSB1cyBhIGNhbGwgb3IgZHJvcCB1cyBhbiBlbWFpbC48L2E+XHJcbiAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwidGV4dC13aGl0ZVwiPjA4LjAwIC0gMTcuMDAgV0lCIChHTVQgKzcuMDApPC9hPlxyXG4gICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cInRleHQtd2hpdGVcIj5TZW5pbiAtIEp1bWF0IChrZWN1YWxpIGhhcmkgbGlidXIpXHJcbiAgICAgICAgICAgICAgTW9uZGF5IC0gRnJpZGF5IChleGNlcHQgaG9saWRheXMpPC9hPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LWZ1bGwgc206dy0xLzQgZmxleCBmbGV4LWNvbCBzcGFjZS15LTRcIj5cclxuICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJ0ZXh0LXdoaXRlXCI+Rk9MTE9XIFNPQ0lBTCBNRURJQTwvYT5cclxuICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJ0ZXh0LXdoaXRlXCI+VFdJVFRFUjwvYT5cclxuICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJ0ZXh0LXdoaXRlXCI+RkFDRUJPT0s8L2E+XHJcbiAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwidGV4dC13aGl0ZVwiPklOU1RBR1JBTTwvYT5cclxuICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJ0ZXh0LXdoaXRlXCI+R09PR0xFPC9hPlxyXG4gICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cInRleHQtd2hpdGVcIj5MSU5LRURJTjwvYT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIHNtOnctMi80IGZsZXggZmxleC1jb2wgc3BhY2UteS00XCI+XHJcbiAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwidGV4dC13aGl0ZVwiPkNPTlRBQ1QgVVM8L2E+XHJcbiAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwidGV4dC13aGl0ZVwiPkdlZHVuZyBCUkkgSUkgTFQuMjMgSmwuSmVuZCBTdWRpcm1hbiBLYXYuNDQtNDYuXHJcbiAgICAgICAgICAgICAgQmVuZHVuZ2FuIEhpbGlyLCBKYWthcnRhIFB1c2F0XHJcbiAgICAgICAgICAgICAgSW5kb25lc2lhLjwvYT5cclxuICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJ0ZXh0LXdoaXRlXCI+UGhvbmU6ICs2MiAyMS0xNTAwLTY4OCk8L2E+XHJcbiAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwidGV4dC13aGl0ZVwiPkVtYWlsOiBjYWxsY2VudGVyQGJyaWRhbmFyZWtzYXNla3VyaXRhcy5jby5pZDwvYT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIHNtOnctMS81IHB0LTYgZmxleCBpdGVtcy1lbmQgbWItMVwiPlxyXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LXJvdyBzcGFjZS14LTRcIj5cclxuICAgICAgICAgICAgICAgIDxpIGNsYXNzTmFtZT1cImZhYiBmYS1mYWNlYm9vay1mXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFiIGZhLXR3aXR0ZXJcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYWIgZmEtaW5zdGFncmFtXCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFiIGZhLWdvb2dsZVwiPjwvaT5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LWNvbFwiPlxyXG4gICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIDxociBzdHlsZT17e2JhY2tncm91bmRDb2xvcjogJyMwNDA5MTknfX0vPlxyXG4gICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwidy1mdWxsIHRleHQtY2VudGVyIG15LTkgdGV4dC1ncmF5LTYwMFwiPkNvcHlyaWdodCDCqSAyMDIxIEJSSSBEQU5BUkVLU0EgU0VLVVJUSUFTPC9wPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuIDwvZm9vdGVyPlxyXG4gICk7XHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEZvb3RlcjtcclxuIl0sIm5hbWVzIjpbIlJlYWN0IiwiTGluayIsIkxvZ29kYW5hcmVrc2EiLCJJbWFnZSIsIk9qayIsIkFrc2VzIiwiSWRjbGVhciIsIklkeCIsIk5hYnVuZ3NhaGFtIiwiS2V1YW5nYW4iLCJGb290ZXIiLCJiYWNrZ3JvdW5kQ29sb3IiXSwic291cmNlUm9vdCI6IiJ9