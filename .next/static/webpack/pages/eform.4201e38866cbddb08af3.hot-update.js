"use strict";
self["webpackHotUpdate_N_E"]("pages/eform",{

/***/ "./components/basic-information/index.js":
/*!***********************************************!*\
  !*** ./components/basic-information/index.js ***!
  \***********************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ BasicInformation; }
/* harmony export */ });
/* harmony import */ var C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-hook-form */ "./node_modules/react-hook-form/dist/index.esm.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__);
/* module decorator */ module = __webpack_require__.hmd(module);


var _jsxFileName = "C:\\NEXTJS\\eform-bri\\components\\basic-information\\index.js",
    _s = $RefreshSig$();

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0,C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_0__.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }






function BasicInformation(props) {
  _s();

  //   const { onSubmit } = props;
  var _useForm = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_2__.useForm)(),
      register = _useForm.register,
      handleSubmit = _useForm.handleSubmit,
      errors = _useForm.formState.errors;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false),
      showReguler = _useState[0],
      setShowReguler = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false),
      showSyariah = _useState2[0],
      setShowSyariah = _useState2[1];

  var _useState3 = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false),
      nasabahBri = _useState3[0],
      setNasabahBri = _useState3[1];

  var _useState4 = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false),
      showOtp = _useState4[0],
      setShowOtp = _useState4[1];

  var _useState5 = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false),
      showNotBri = _useState5[0],
      setShowNotBri = _useState5[1];

  var onSubmit = function onSubmit(data) {
    return submitData(data);
  };

  var handleChangeTypeRek = function handleChangeTypeRek(e) {
    console.log(e.target.value);

    if (e.target.value === 'Reguler') {
      setShowReguler(true);
      setShowSyariah(false);
    } else if (e.target.value === 'Syariah') {
      setShowReguler(false);
      setShowSyariah(true);
      setNasabahBri(false);
      setShowOtp(false);
      setShowNotBri(false);
    } else {
      setShowReguler(false);
      setShowSyariah(false);
    }
  };

  var handleChangeReferensi = function handleChangeReferensi(e) {
    if (e.target.value === 'ya') {
      setNasabahBri(true);
    } else {
      setNasabahBri(false);
      setShowOtp(false);
      setShowNotBri(false);
    }
  };

  var handleChangeShowOtp = function handleChangeShowOtp(e) {
    if (e.target.value === 'ya') {
      setShowOtp(true);
      setShowNotBri(false);
    } else {
      setShowOtp(false);
      setShowNotBri(true);
    }
  };

  var submitData = function submitData(data) {
    console.log(data, 'datas');
    localStorage.setItem('nextStepper1', 'true');
  };

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
    className: "border-solid mx-auto bg-white overflow-hidden border-2 mt-20 w-3/5",
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
      className: "md:flex bg-blue-450 m-8 rounded-2xl",
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        style: styles.card,
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("p", {
          style: styles.title,
          children: "ALAMAT PEMOHON"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 64,
          columnNumber: 37
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 63,
        columnNumber: 33
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 62,
      columnNumber: 29
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("form", {
      id: "my-form",
      className: "ml-8 mr-8",
      onSubmit: handleSubmit(onSubmit),
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-6",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("label", {
            className: "block tracking-wide text-gray-700 text-xs font-bold mb-2",
            children: ["Branch (SID) ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("span", {
              style: {
                color: "red"
              },
              children: "*"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 73,
              columnNumber: 57
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 72,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 71,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "relative",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("select", _objectSpread(_objectSpread({}, register("branchId", {
              required: true
            })), {}, {
              name: "branchId",
              className: "border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: ""
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 80,
                columnNumber: 43
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "B0394",
                children: "B0394"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 81,
                columnNumber: 43
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "B0396",
                children: "B0396"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 82,
                columnNumber: 43
              }, this)]
            }), void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 78,
              columnNumber: 41
            }, this), errors.branchId && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
              className: "mb-3 text-normal text-red-500 ",
              children: "Field is required"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 84,
              columnNumber: 62
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 77,
            columnNumber: 37
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 76,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 70,
        columnNumber: 33
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-6",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("label", {
            className: "block tracking-wide text-gray-700 text-xs font-bold mb-2",
            children: "Referal Code"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 90,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 89,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("input", _objectSpread({
            className: "border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700",
            name: "referalCode",
            autoFocus: true
          }, register("referalCode")), void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 95,
            columnNumber: 41
          }, this), errors.referalCode && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "mb-3 text-normal text-red-500 ",
            children: "Field is required"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 101,
            columnNumber: 65
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 94,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 33
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-6",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("label", {
            className: "block tracking-wide text-gray-700 text-xs font-bold mb-2",
            children: ["Jenis rekening yang Anda inginkan ? ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("span", {
              style: {
                color: "red"
              },
              children: "*"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 107,
              columnNumber: 80
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 106,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 105,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "relative",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("select", _objectSpread(_objectSpread({}, register("typeRek", {
              required: true
            })), {}, {
              name: "typeRek",
              className: "border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700",
              onChange: handleChangeTypeRek,
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: ""
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 114,
                columnNumber: 43
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "Reguler",
                children: "Reguler"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 115,
                columnNumber: 43
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "Syariah",
                children: "Syariah"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 116,
                columnNumber: 43
              }, this)]
            }), void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 112,
              columnNumber: 41
            }, this), errors.typeRek && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
              className: "mb-3 text-normal text-red-500 ",
              children: "Field is required"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 118,
              columnNumber: 61
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 111,
            columnNumber: 37
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 110,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 104,
        columnNumber: 33
      }, this), showReguler ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-6",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("label", {
            className: "block tracking-wide text-gray-700 text-xs font-bold mb-2",
            children: ["Apakah anda mendapat referensi dari BRI ? ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("span", {
              style: {
                color: "red"
              },
              children: "*"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 126,
              columnNumber: 83
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 125,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 124,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "relative",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("select", _objectSpread(_objectSpread({}, register("isReferensiBri", {
              required: true
            })), {}, {
              name: "isReferensiBri",
              className: "border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700",
              onChange: handleChangeReferensi,
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: ""
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 133,
                columnNumber: 41
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "ya",
                children: "YA"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 134,
                columnNumber: 41
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "tidak",
                children: "TIDAK"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 135,
                columnNumber: 41
              }, this)]
            }), void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 131,
              columnNumber: 41
            }, this), errors.isReferensiBri && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
              className: "mb-3 text-normal text-red-500 ",
              children: "Field is required"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 137,
              columnNumber: 68
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 130,
            columnNumber: 37
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 129,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 123,
        columnNumber: 33
      }, this) : null, nasabahBri ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-6",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("label", {
            className: "block tracking-wide text-gray-700 text-xs font-bold mb-2",
            children: ["Apakah saat ini Anda sebagai nasabah BRI? ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("span", {
              style: {
                color: "red"
              },
              children: "*"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 145,
              columnNumber: 83
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 143,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "relative",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("select", _objectSpread(_objectSpread({}, register("isNasabahBri", {
              required: true
            })), {}, {
              name: "isNasabahBri",
              className: "border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700",
              onChange: handleChangeShowOtp,
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: ""
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 152,
                columnNumber: 41
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "ya",
                children: "YA"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 153,
                columnNumber: 41
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("option", {
                value: "tidak",
                children: "TIDAK"
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 154,
                columnNumber: 41
              }, this)]
            }), void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 150,
              columnNumber: 41
            }, this), errors.isNasabahBri && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
              className: "mb-3 text-normal text-red-500 ",
              children: "Field is required"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 156,
              columnNumber: 66
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 149,
            columnNumber: 37
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 148,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 142,
        columnNumber: 33
      }, this) : null, /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-2",
        children: [showSyariah ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "md:flex rounded-2xl items-center px-16",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            style: styles.card,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("p", {
              style: {
                textAlign: "center"
              },
              className: "block tracking-wide text-gray-700 text-xs font-bold",
              children: ["DSN-MUI No. 80/DSN-MUI/III/2011 - Penerapan Prinsip Syariah Perdagangan Efek Ekuitas, silahkan klik ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_3___default()), {
                href: "/auth/login",
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("span", {
                  style: {
                    color: "blue"
                  },
                  children: "disini"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 167,
                  columnNumber: 170
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 167,
                columnNumber: 145
              }, this), " dan fatwa ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_3___default()), {
                href: "/auth/login",
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("span", {
                  style: {
                    color: "blue"
                  },
                  children: "lainnya."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 167,
                  columnNumber: 255
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 167,
                columnNumber: 230
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 166,
              columnNumber: 41
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 165,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 164,
          columnNumber: 37
        }, this) : null, showNotBri ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "md:flex rounded-2xl items-center px-16",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            style: styles.card,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("p", {
              style: {
                textAlign: "center"
              },
              className: "block tracking-wide text-gray-700 text-xs font-bold",
              children: ["Untuk melanjutkan proses pembukuaan rekening BRI Danareksa, kami sarankan Anda untuk membuka rekening Bank BRI melalui link berikut ini :", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)((next_link__WEBPACK_IMPORTED_MODULE_3___default()), {
                href: "/auth/login",
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("span", {
                  style: {
                    color: "blue"
                  },
                  children: "bukarekening.bri.co.id"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 178,
                  columnNumber: 70
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 178,
                columnNumber: 45
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 174,
              columnNumber: 41
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 173,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 172,
          columnNumber: 37
        }, this) : null]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 33
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-2",
        children: showOtp ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "md:flex rounded-2xl items-center px-16",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            style: styles.card,
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("p", {
              style: {
                textAlign: "center"
              },
              className: "block tracking-wide text-gray-700 text-xs font-bold",
              children: "Pastikan Nomor Handphone dan Nomor Rekening Anda sudah terdaftar di BRI. Nomor Hanphone dan Nomor Rekening BRI akan kami kirimkan ke BRI untuk proses verifikasi."
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 187,
              columnNumber: 41
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 186,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 185,
          columnNumber: 37
        }, this) : null
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 183,
        columnNumber: 33
      }, this), showOtp ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-6",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("label", {
            className: "block tracking-wide text-gray-700 text-xs font-bold mb-2",
            children: "No Handphone"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 194,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 193,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("input", _objectSpread({
            className: "border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700",
            name: "noHp",
            autoFocus: true
          }, register("noHp", {
            required: true,
            pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/
          })), void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 199,
            columnNumber: 37
          }, this), console.log(errors.noHp), errors.noHp && errors.noHp.type === "required" && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "mb-3 text-normal text-red-500",
            children: "Field is required"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 205,
            columnNumber: 91
          }, this), errors.noHp && errors.noHp.type === "pattern" && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "mb-3 text-normal text-red-500",
            children: "Field is Number"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 206,
            columnNumber: 90
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 198,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 192,
        columnNumber: 33
      }, this) : null, showOtp ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex flex-wrap -mx-3 mb-6",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("label", {
            className: "block tracking-wide text-gray-700 text-xs font-bold mb-2",
            children: "No Rekening BRI"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 212,
            columnNumber: 41
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 211,
          columnNumber: 37
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
          className: "w-full md:w-1/2 px-3 w-2/4",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("input", _objectSpread({
            className: "border-solid border-gray-500 border py-2 px-4 w-full rounded text-gray-700",
            name: "NoRekBri",
            autoFocus: true
          }, register("NoRekBri", {
            required: true,
            pattern: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/
          })), void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 217,
            columnNumber: 37
          }, this), errors.NoRekBri && errors.NoRekBri.type === "required" && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "mb-3 text-normal text-red-500",
            children: "Field is required"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 222,
            columnNumber: 99
          }, this), errors.NoRekBri && errors.NoRekBri.type === "pattern" && /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
            className: "mb-3 text-normal text-red-500 ",
            children: "Field is Number"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 223,
            columnNumber: 98
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 216,
          columnNumber: 37
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 210,
        columnNumber: 33
      }, this) : null, showOtp ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("div", {
        className: "flex justify-center mb-3",
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxDEV)("button", {
          className: "bg-yellow-500 text-white font-bold py-2 px-4 rounded",
          type: "submit",
          onClick: true,
          children: "Kirim OTP"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 228,
          columnNumber: 38
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 227,
        columnNumber: 38
      }, this) : null]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 68,
      columnNumber: 29
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 61,
    columnNumber: 5
  }, this);
}

_s(BasicInformation, "sdsas7xn3Ly+twpIwsvNm42ZCSA=", false, function () {
  return [react_hook_form__WEBPACK_IMPORTED_MODULE_2__.useForm];
});

_c = BasicInformation;
var styles = {
  container: {
    minHeight: 'inherit',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  card: {
    margin: 25
  },
  title: {
    color: "#FFFFFF"
  },
  banner: {
    pt: ['140px', '145px', '155px', '170px', null, null, '180px', '215px'],
    pb: [2, null, 0, null, 2, 0, null, 5],
    position: 'relative',
    zIndex: 2,
    '&::before': {
      position: 'absolute',
      content: '""',
      bottom: 6,
      left: 0,
      height: '100%',
      width: '100%',
      zIndex: -1,
      backgroundRepeat: "no-repeat",
      backgroundPosition: 'bottom left',
      backgroundSize: '36%'
    },
    '&::after': {
      position: 'absolute',
      content: '""',
      bottom: '40px',
      right: 0,
      height: '100%',
      width: '100%',
      zIndex: -1,
      backgroundRepeat: "no-repeat",
      backgroundPosition: 'bottom right',
      backgroundSize: '32%'
    },
    container: {
      minHeight: 'inherit',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center'
    },
    contentBox: {
      width: ['100%', '90%', '535px', null, '57%', '60%', '68%', '60%'],
      mx: 'auto',
      textAlign: 'center',
      mb: ['40px', null, null, null, null, 7]
    },
    imageBox: {
      justifyContent: 'center',
      textAlign: 'center',
      display: 'inline-flex',
      mb: [0, null, -6, null, null, '10px', null, -3],
      img: {
        position: 'relative',
        height: [1000]
      }
    }
  }
};

var _c;

$RefreshReg$(_c, "BasicInformation");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvZWZvcm0uNDIwMWUzODg2NmNiZGRiMDhhZjMuaG90LXVwZGF0ZS5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBRWUsU0FBU0ksZ0JBQVQsQ0FBMEJDLEtBQTFCLEVBQWlDO0FBQUE7O0FBQ2hEO0FBQ0UsaUJBQTBESix3REFBTyxFQUFqRTtBQUFBLE1BQVFLLFFBQVIsWUFBUUEsUUFBUjtBQUFBLE1BQWtCQyxZQUFsQixZQUFrQkEsWUFBbEI7QUFBQSxNQUE2Q0MsTUFBN0MsWUFBZ0NDLFNBQWhDLENBQTZDRCxNQUE3Qzs7QUFDQSxrQkFBd0NOLCtDQUFRLENBQUMsS0FBRCxDQUFoRDtBQUFBLE1BQVFRLFdBQVI7QUFBQSxNQUFxQkMsY0FBckI7O0FBQ0EsbUJBQXdDVCwrQ0FBUSxDQUFDLEtBQUQsQ0FBaEQ7QUFBQSxNQUFRVSxXQUFSO0FBQUEsTUFBcUJDLGNBQXJCOztBQUNBLG1CQUFzQ1gsK0NBQVEsQ0FBQyxLQUFELENBQTlDO0FBQUEsTUFBUVksVUFBUjtBQUFBLE1BQW9CQyxhQUFwQjs7QUFDQSxtQkFBZ0NiLCtDQUFRLENBQUMsS0FBRCxDQUF4QztBQUFBLE1BQVFjLE9BQVI7QUFBQSxNQUFpQkMsVUFBakI7O0FBQ0EsbUJBQXNDZiwrQ0FBUSxDQUFDLEtBQUQsQ0FBOUM7QUFBQSxNQUFRZ0IsVUFBUjtBQUFBLE1BQW9CQyxhQUFwQjs7QUFFQSxNQUFNQyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFBQyxJQUFJO0FBQUEsV0FBSUMsVUFBVSxDQUFDRCxJQUFELENBQWQ7QUFBQSxHQUFyQjs7QUFFQSxNQUFNRSxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUNDLENBQUQsRUFBTztBQUMvQkMsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVlGLENBQUMsQ0FBQ0csTUFBRixDQUFTQyxLQUFyQjs7QUFDRSxRQUFHSixDQUFDLENBQUNHLE1BQUYsQ0FBU0MsS0FBVCxLQUFtQixTQUF0QixFQUFnQztBQUM1QmpCLE1BQUFBLGNBQWMsQ0FBQyxJQUFELENBQWQ7QUFDQUUsTUFBQUEsY0FBYyxDQUFDLEtBQUQsQ0FBZDtBQUNILEtBSEQsTUFHTyxJQUFHVyxDQUFDLENBQUNHLE1BQUYsQ0FBU0MsS0FBVCxLQUFtQixTQUF0QixFQUFpQztBQUNwQ2pCLE1BQUFBLGNBQWMsQ0FBQyxLQUFELENBQWQ7QUFDQUUsTUFBQUEsY0FBYyxDQUFDLElBQUQsQ0FBZDtBQUNBRSxNQUFBQSxhQUFhLENBQUMsS0FBRCxDQUFiO0FBQ0FFLE1BQUFBLFVBQVUsQ0FBQyxLQUFELENBQVY7QUFDQUUsTUFBQUEsYUFBYSxDQUFDLEtBQUQsQ0FBYjtBQUNILEtBTk0sTUFNQTtBQUNIUixNQUFBQSxjQUFjLENBQUMsS0FBRCxDQUFkO0FBQ0FFLE1BQUFBLGNBQWMsQ0FBQyxLQUFELENBQWQ7QUFDSDtBQUNOLEdBZkQ7O0FBaUJBLE1BQU1nQixxQkFBcUIsR0FBRyxTQUF4QkEscUJBQXdCLENBQUFMLENBQUMsRUFBSTtBQUNqQyxRQUFHQSxDQUFDLENBQUNHLE1BQUYsQ0FBU0MsS0FBVCxLQUFtQixJQUF0QixFQUEyQjtBQUN2QmIsTUFBQUEsYUFBYSxDQUFDLElBQUQsQ0FBYjtBQUNILEtBRkQsTUFFTztBQUNIQSxNQUFBQSxhQUFhLENBQUMsS0FBRCxDQUFiO0FBQ0FFLE1BQUFBLFVBQVUsQ0FBQyxLQUFELENBQVY7QUFDQUUsTUFBQUEsYUFBYSxDQUFDLEtBQUQsQ0FBYjtBQUNIO0FBQ0YsR0FSRDs7QUFVQSxNQUFNVyxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQUFOLENBQUMsRUFBSTtBQUMvQixRQUFHQSxDQUFDLENBQUNHLE1BQUYsQ0FBU0MsS0FBVCxLQUFtQixJQUF0QixFQUEyQjtBQUN2QlgsTUFBQUEsVUFBVSxDQUFDLElBQUQsQ0FBVjtBQUNBRSxNQUFBQSxhQUFhLENBQUMsS0FBRCxDQUFiO0FBQ0gsS0FIRCxNQUdPO0FBQ0hGLE1BQUFBLFVBQVUsQ0FBQyxLQUFELENBQVY7QUFDQUUsTUFBQUEsYUFBYSxDQUFDLElBQUQsQ0FBYjtBQUNIO0FBQ0YsR0FSRDs7QUFVQSxNQUFNRyxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDRCxJQUFELEVBQVU7QUFDM0JJLElBQUFBLE9BQU8sQ0FBQ0MsR0FBUixDQUFZTCxJQUFaLEVBQWtCLE9BQWxCO0FBQ0FVLElBQUFBLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixjQUFyQixFQUFxQyxNQUFyQztBQUNELEdBSEQ7O0FBTUEsc0JBQ0U7QUFBSyxhQUFTLEVBQUMsb0VBQWY7QUFBQSw0QkFDd0I7QUFBSyxlQUFTLEVBQUMscUNBQWY7QUFBQSw2QkFDSTtBQUFLLGFBQUssRUFBRUMsTUFBTSxDQUFDQyxJQUFuQjtBQUFBLCtCQUNJO0FBQUcsZUFBSyxFQUFFRCxNQUFNLENBQUNFLEtBQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFEeEIsZUFPd0I7QUFBTSxRQUFFLEVBQUMsU0FBVDtBQUFtQixlQUFTLEVBQUMsV0FBN0I7QUFDSSxjQUFRLEVBQUU1QixZQUFZLENBQUNhLFFBQUQsQ0FEMUI7QUFBQSw4QkFFSTtBQUFLLGlCQUFTLEVBQUMsMkJBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxpQ0FDSTtBQUFPLHFCQUFTLEVBQUMsMERBQWpCO0FBQUEscURBQ2dCO0FBQU0sbUJBQUssRUFBRTtBQUFDZ0IsZ0JBQUFBLEtBQUssRUFBQztBQUFQLGVBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRGhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFNSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxpQ0FDQTtBQUFLLHFCQUFTLEVBQUMsVUFBZjtBQUFBLG9DQUNJLHdHQUFZOUIsUUFBUSxDQUFDLFVBQUQsRUFBWTtBQUFDK0IsY0FBQUEsUUFBUSxFQUFFO0FBQVgsYUFBWixDQUFwQjtBQUFtRCxrQkFBSSxFQUFDLFVBQXhEO0FBQW1FLHVCQUFTLEVBQUMsNEVBQTdFO0FBQUEsc0NBRUU7QUFBUSxxQkFBSyxFQUFDO0FBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFGRixlQUdFO0FBQVEscUJBQUssRUFBQyxPQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUhGLGVBSUU7QUFBUSxxQkFBSyxFQUFDLE9BQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURKLEVBT0s3QixNQUFNLENBQUM4QixRQUFQLGlCQUFvQjtBQUFLLHVCQUFTLEVBQUMsZ0NBQWY7QUFBQSx3QkFBaUQ7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFQekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGSixlQW9CSTtBQUFLLGlCQUFTLEVBQUMsMkJBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxpQ0FDSTtBQUFPLHFCQUFTLEVBQUMsMERBQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQU1JO0FBQUssbUJBQVMsRUFBQyw0QkFBZjtBQUFBLGtDQUNJO0FBQ0EscUJBQVMsRUFBQyw0RUFEVjtBQUVBLGdCQUFJLEVBQUMsYUFGTDtBQUlBLHFCQUFTO0FBSlQsYUFLSWhDLFFBQVEsQ0FBQyxhQUFELENBTFo7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFESixFQU9LRSxNQUFNLENBQUMrQixXQUFQLGlCQUF1QjtBQUFLLHFCQUFTLEVBQUMsZ0NBQWY7QUFBQSxzQkFBaUQ7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFQNUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXBCSixlQW9DSTtBQUFLLGlCQUFTLEVBQUMsMkJBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxpQ0FDSTtBQUFPLHFCQUFTLEVBQUMsMERBQWpCO0FBQUEsNEVBQ3VDO0FBQU0sbUJBQUssRUFBRTtBQUFDSCxnQkFBQUEsS0FBSyxFQUFDO0FBQVAsZUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFEdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQU1JO0FBQUssbUJBQVMsRUFBQyw0QkFBZjtBQUFBLGlDQUNBO0FBQUsscUJBQVMsRUFBQyxVQUFmO0FBQUEsb0NBQ0ksd0dBQVk5QixRQUFRLENBQUMsU0FBRCxFQUFXO0FBQUMrQixjQUFBQSxRQUFRLEVBQUU7QUFBWCxhQUFYLENBQXBCO0FBQWtELGtCQUFJLEVBQUMsU0FBdkQ7QUFBaUUsdUJBQVMsRUFBQyw0RUFBM0U7QUFDQSxzQkFBUSxFQUFFZCxtQkFEVjtBQUFBLHNDQUVFO0FBQVEscUJBQUssRUFBQztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRkYsZUFHRTtBQUFRLHFCQUFLLEVBQUMsU0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFIRixlQUlFO0FBQVEscUJBQUssRUFBQyxTQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFESixFQU9LZixNQUFNLENBQUNnQyxPQUFQLGlCQUFtQjtBQUFLLHVCQUFTLEVBQUMsZ0NBQWY7QUFBQSx3QkFBaUQ7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFQeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FwQ0osRUFzREs5QixXQUFXLGdCQUNaO0FBQUssaUJBQVMsRUFBQywyQkFBZjtBQUFBLGdDQUNJO0FBQUssbUJBQVMsRUFBQyw0QkFBZjtBQUFBLGlDQUNJO0FBQU8scUJBQVMsRUFBQywwREFBakI7QUFBQSxrRkFDMEM7QUFBTSxtQkFBSyxFQUFFO0FBQUMwQixnQkFBQUEsS0FBSyxFQUFDO0FBQVAsZUFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFEMUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQU1JO0FBQUssbUJBQVMsRUFBQyw0QkFBZjtBQUFBLGlDQUNBO0FBQUsscUJBQVMsRUFBQyxVQUFmO0FBQUEsb0NBQ0ksd0dBQVk5QixRQUFRLENBQUMsZ0JBQUQsRUFBa0I7QUFBQytCLGNBQUFBLFFBQVEsRUFBRTtBQUFYLGFBQWxCLENBQXBCO0FBQXlELGtCQUFJLEVBQUMsZ0JBQTlEO0FBQStFLHVCQUFTLEVBQUMsNEVBQXpGO0FBQ0Esc0JBQVEsRUFBRVIscUJBRFY7QUFBQSxzQ0FFQTtBQUFRLHFCQUFLLEVBQUM7QUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUZBLGVBR0E7QUFBUSxxQkFBSyxFQUFDLElBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBSEEsZUFJQTtBQUFRLHFCQUFLLEVBQUMsT0FBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFKQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREosRUFPS3JCLE1BQU0sQ0FBQ2lDLGNBQVAsaUJBQTBCO0FBQUssdUJBQVMsRUFBQyxnQ0FBZjtBQUFBLHdCQUFpRDtBQUFqRDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQVAvQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURZLEdBa0JILElBeEViLEVBeUVLM0IsVUFBVSxnQkFDWDtBQUFLLGlCQUFTLEVBQUMsMkJBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxpQ0FDSTtBQUFPLHFCQUFTLEVBQUMsMERBQWpCO0FBQUEsa0ZBQzBDO0FBQU0sbUJBQUssRUFBRTtBQUFDc0IsZ0JBQUFBLEtBQUssRUFBQztBQUFQLGVBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRDFDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFNSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxpQ0FDQTtBQUFLLHFCQUFTLEVBQUMsVUFBZjtBQUFBLG9DQUNJLHdHQUFZOUIsUUFBUSxDQUFDLGNBQUQsRUFBZ0I7QUFBQytCLGNBQUFBLFFBQVEsRUFBRTtBQUFYLGFBQWhCLENBQXBCO0FBQXVELGtCQUFJLEVBQUMsY0FBNUQ7QUFBMkUsdUJBQVMsRUFBQyw0RUFBckY7QUFDQSxzQkFBUSxFQUFFUCxtQkFEVjtBQUFBLHNDQUVBO0FBQVEscUJBQUssRUFBQztBQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0JBRkEsZUFHQTtBQUFRLHFCQUFLLEVBQUMsSUFBZDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFIQSxlQUlBO0FBQVEscUJBQUssRUFBQyxPQUFkO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUpBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFESixFQU9LdEIsTUFBTSxDQUFDa0MsWUFBUCxpQkFBd0I7QUFBSyx1QkFBUyxFQUFDLGdDQUFmO0FBQUEsd0JBQWlEO0FBQWpEO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBUDdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRFcsR0FtQkYsSUE1RmIsZUE4Rkk7QUFBSyxpQkFBUyxFQUFDLDJCQUFmO0FBQUEsbUJBQ0M5QixXQUFXLGdCQUNSO0FBQUssbUJBQVMsRUFBQyx3Q0FBZjtBQUFBLGlDQUNJO0FBQUssaUJBQUssRUFBRXFCLE1BQU0sQ0FBQ0MsSUFBbkI7QUFBQSxtQ0FDQTtBQUFHLG1CQUFLLEVBQUU7QUFBQ1MsZ0JBQUFBLFNBQVMsRUFBQztBQUFYLGVBQVY7QUFBZ0MsdUJBQVMsRUFBQyxxREFBMUM7QUFBQSw4SUFDd0csOERBQUMsa0RBQUQ7QUFBTSxvQkFBSSxFQUFDLGFBQVg7QUFBQSx1Q0FBeUI7QUFBTSx1QkFBSyxFQUFFO0FBQUNQLG9CQUFBQSxLQUFLLEVBQUM7QUFBUCxtQkFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF6QjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUR4Ryw4QkFDNkwsOERBQUMsa0RBQUQ7QUFBTSxvQkFBSSxFQUFDLGFBQVg7QUFBQSx1Q0FBeUI7QUFBTSx1QkFBSyxFQUFFO0FBQUNBLG9CQUFBQSxLQUFLLEVBQUM7QUFBUCxtQkFBYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF6QjtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQUQ3TDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFEUSxHQU9DLElBUmIsRUFTQ2xCLFVBQVUsZ0JBQ1A7QUFBSyxtQkFBUyxFQUFDLHdDQUFmO0FBQUEsaUNBQ0k7QUFBSyxpQkFBSyxFQUFFZSxNQUFNLENBQUNDLElBQW5CO0FBQUEsbUNBQ0E7QUFBRyxtQkFBSyxFQUFFO0FBQUNTLGdCQUFBQSxTQUFTLEVBQUM7QUFBWCxlQUFWO0FBQ0EsdUJBQVMsRUFBQyxxREFEVjtBQUFBLG1MQUlJLDhEQUFDLGtEQUFEO0FBQU0sb0JBQUksRUFBQyxhQUFYO0FBQUEsdUNBQXlCO0FBQU0sdUJBQUssRUFBRTtBQUFDUCxvQkFBQUEsS0FBSyxFQUFDO0FBQVAsbUJBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBekI7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQkFKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFETyxHQVVFLElBbkJiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQTlGSixlQW1ISTtBQUFLLGlCQUFTLEVBQUMsMkJBQWY7QUFBQSxrQkFDQ3BCLE9BQU8sZ0JBQ0o7QUFBSyxtQkFBUyxFQUFDLHdDQUFmO0FBQUEsaUNBQ0k7QUFBSyxpQkFBSyxFQUFFaUIsTUFBTSxDQUFDQyxJQUFuQjtBQUFBLG1DQUNBO0FBQUcsbUJBQUssRUFBRTtBQUFDUyxnQkFBQUEsU0FBUyxFQUFDO0FBQVgsZUFBVjtBQUFnQyx1QkFBUyxFQUFDLHFEQUExQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURJLEdBS0s7QUFOYjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBbkhKLEVBMkhLM0IsT0FBTyxnQkFDUjtBQUFLLGlCQUFTLEVBQUMsMkJBQWY7QUFBQSxnQ0FDSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxpQ0FDSTtBQUFPLHFCQUFTLEVBQUMsMERBQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFESixlQU1JO0FBQUssbUJBQVMsRUFBQyw0QkFBZjtBQUFBLGtDQUNBO0FBQ0kscUJBQVMsRUFBQyw0RUFEZDtBQUVJLGdCQUFJLEVBQUMsTUFGVDtBQUdJLHFCQUFTO0FBSGIsYUFJUVYsUUFBUSxDQUFDLE1BQUQsRUFBUTtBQUFDK0IsWUFBQUEsUUFBUSxFQUFFLElBQVg7QUFBaUJPLFlBQUFBLE9BQU8sRUFBRTtBQUExQixXQUFSLENBSmhCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREEsRUFNS25CLE9BQU8sQ0FBQ0MsR0FBUixDQUFZbEIsTUFBTSxDQUFDcUMsSUFBbkIsQ0FOTCxFQU9LckMsTUFBTSxDQUFDcUMsSUFBUCxJQUFlckMsTUFBTSxDQUFDcUMsSUFBUCxDQUFZQyxJQUFaLEtBQW1CLFVBQWxDLGlCQUFpRDtBQUFLLHFCQUFTLEVBQUMsK0JBQWY7QUFBQSxzQkFBZ0Q7QUFBaEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFQdEQsRUFRS3RDLE1BQU0sQ0FBQ3FDLElBQVAsSUFBZXJDLE1BQU0sQ0FBQ3FDLElBQVAsQ0FBWUMsSUFBWixLQUFtQixTQUFsQyxpQkFBZ0Q7QUFBSyxxQkFBUyxFQUFDLCtCQUFmO0FBQUEsc0JBQWdEO0FBQWhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBUnJEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFOSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FEUSxHQWlCQyxJQTVJYixFQTZJSzlCLE9BQU8sZ0JBQ1I7QUFBSyxpQkFBUyxFQUFDLDJCQUFmO0FBQUEsZ0NBQ0k7QUFBSyxtQkFBUyxFQUFDLDRCQUFmO0FBQUEsaUNBQ0k7QUFBTyxxQkFBUyxFQUFDLDBEQUFqQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREosZUFNSTtBQUFLLG1CQUFTLEVBQUMsNEJBQWY7QUFBQSxrQ0FDQTtBQUNJLHFCQUFTLEVBQUMsNEVBRGQ7QUFFSSxnQkFBSSxFQUFDLFVBRlQ7QUFHSSxxQkFBUztBQUhiLGFBSVFWLFFBQVEsQ0FBQyxVQUFELEVBQVk7QUFBQytCLFlBQUFBLFFBQVEsRUFBRSxJQUFYO0FBQWlCTyxZQUFBQSxPQUFPLEVBQUU7QUFBMUIsV0FBWixDQUpoQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURBLEVBTUtwQyxNQUFNLENBQUN1QyxRQUFQLElBQW1CdkMsTUFBTSxDQUFDdUMsUUFBUCxDQUFnQkQsSUFBaEIsS0FBdUIsVUFBMUMsaUJBQXlEO0FBQUsscUJBQVMsRUFBQywrQkFBZjtBQUFBLHNCQUFnRDtBQUFoRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQU45RCxFQU9LdEMsTUFBTSxDQUFDdUMsUUFBUCxJQUFtQnZDLE1BQU0sQ0FBQ3VDLFFBQVAsQ0FBZ0JELElBQWhCLEtBQXVCLFNBQTFDLGlCQUF3RDtBQUFLLHFCQUFTLEVBQUMsZ0NBQWY7QUFBQSxzQkFBaUQ7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFQN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQU5KO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURRLEdBZ0JDLElBN0piLEVBOEpLOUIsT0FBTyxnQkFDSDtBQUFLLGlCQUFTLEVBQUMsMEJBQWY7QUFBQSwrQkFDQTtBQUFRLG1CQUFTLEVBQUMsc0RBQWxCO0FBQ0MsY0FBSSxFQUFDLFFBRE47QUFDZSxpQkFBTyxNQUR0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERyxHQU1NLElBcEtsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFQeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFnTEQ7O0dBdE91Qlo7VUFFb0NIOzs7S0FGcENHO0FBd094QixJQUFNNkIsTUFBTSxHQUFHO0FBQ2JlLEVBQUFBLFNBQVMsRUFBRTtBQUNQQyxJQUFBQSxTQUFTLEVBQUUsU0FESjtBQUVQQyxJQUFBQSxPQUFPLEVBQUUsTUFGRjtBQUdQQyxJQUFBQSxhQUFhLEVBQUUsUUFIUjtBQUlQQyxJQUFBQSxjQUFjLEVBQUU7QUFKVCxHQURFO0FBT2JsQixFQUFBQSxJQUFJLEVBQUU7QUFDRm1CLElBQUFBLE1BQU0sRUFBRTtBQUROLEdBUE87QUFVYmxCLEVBQUFBLEtBQUssRUFBRTtBQUNIQyxJQUFBQSxLQUFLLEVBQUU7QUFESixHQVZNO0FBWVZrQixFQUFBQSxNQUFNLEVBQUU7QUFDUEMsSUFBQUEsRUFBRSxFQUFFLENBQUMsT0FBRCxFQUFVLE9BQVYsRUFBbUIsT0FBbkIsRUFBNEIsT0FBNUIsRUFBcUMsSUFBckMsRUFBMkMsSUFBM0MsRUFBaUQsT0FBakQsRUFBMEQsT0FBMUQsQ0FERztBQUVQQyxJQUFBQSxFQUFFLEVBQUUsQ0FBQyxDQUFELEVBQUksSUFBSixFQUFVLENBQVYsRUFBYSxJQUFiLEVBQW1CLENBQW5CLEVBQXNCLENBQXRCLEVBQXlCLElBQXpCLEVBQStCLENBQS9CLENBRkc7QUFHUEMsSUFBQUEsUUFBUSxFQUFFLFVBSEg7QUFJUEMsSUFBQUEsTUFBTSxFQUFFLENBSkQ7QUFLUCxpQkFBYTtBQUNURCxNQUFBQSxRQUFRLEVBQUUsVUFERDtBQUVURSxNQUFBQSxPQUFPLEVBQUUsSUFGQTtBQUdUQyxNQUFBQSxNQUFNLEVBQUUsQ0FIQztBQUlUQyxNQUFBQSxJQUFJLEVBQUUsQ0FKRztBQUtUQyxNQUFBQSxNQUFNLEVBQUUsTUFMQztBQU1UQyxNQUFBQSxLQUFLLEVBQUUsTUFORTtBQU9UTCxNQUFBQSxNQUFNLEVBQUUsQ0FBQyxDQVBBO0FBUVRNLE1BQUFBLGdCQUFnQixhQVJQO0FBU1RDLE1BQUFBLGtCQUFrQixFQUFFLGFBVFg7QUFVVEMsTUFBQUEsY0FBYyxFQUFFO0FBVlAsS0FMTjtBQWlCUCxnQkFBWTtBQUNSVCxNQUFBQSxRQUFRLEVBQUUsVUFERjtBQUVSRSxNQUFBQSxPQUFPLEVBQUUsSUFGRDtBQUdSQyxNQUFBQSxNQUFNLEVBQUUsTUFIQTtBQUlSTyxNQUFBQSxLQUFLLEVBQUUsQ0FKQztBQUtSTCxNQUFBQSxNQUFNLEVBQUUsTUFMQTtBQU1SQyxNQUFBQSxLQUFLLEVBQUUsTUFOQztBQU9STCxNQUFBQSxNQUFNLEVBQUUsQ0FBQyxDQVBEO0FBUVJNLE1BQUFBLGdCQUFnQixhQVJSO0FBU1JDLE1BQUFBLGtCQUFrQixFQUFFLGNBVFo7QUFVUkMsTUFBQUEsY0FBYyxFQUFFO0FBVlIsS0FqQkw7QUE2QlBsQixJQUFBQSxTQUFTLEVBQUU7QUFDUEMsTUFBQUEsU0FBUyxFQUFFLFNBREo7QUFFUEMsTUFBQUEsT0FBTyxFQUFFLE1BRkY7QUFHUEMsTUFBQUEsYUFBYSxFQUFFLFFBSFI7QUFJUEMsTUFBQUEsY0FBYyxFQUFFO0FBSlQsS0E3Qko7QUFtQ1BnQixJQUFBQSxVQUFVLEVBQUU7QUFDUkwsTUFBQUEsS0FBSyxFQUFFLENBQUMsTUFBRCxFQUFTLEtBQVQsRUFBZ0IsT0FBaEIsRUFBeUIsSUFBekIsRUFBK0IsS0FBL0IsRUFBc0MsS0FBdEMsRUFBNkMsS0FBN0MsRUFBb0QsS0FBcEQsQ0FEQztBQUVSTSxNQUFBQSxFQUFFLEVBQUUsTUFGSTtBQUdSMUIsTUFBQUEsU0FBUyxFQUFFLFFBSEg7QUFJUjJCLE1BQUFBLEVBQUUsRUFBRSxDQUFDLE1BQUQsRUFBUyxJQUFULEVBQWUsSUFBZixFQUFxQixJQUFyQixFQUEyQixJQUEzQixFQUFpQyxDQUFqQztBQUpJLEtBbkNMO0FBeUNQQyxJQUFBQSxRQUFRLEVBQUU7QUFDTm5CLE1BQUFBLGNBQWMsRUFBRSxRQURWO0FBRU5ULE1BQUFBLFNBQVMsRUFBRSxRQUZMO0FBR05PLE1BQUFBLE9BQU8sRUFBRSxhQUhIO0FBSU5vQixNQUFBQSxFQUFFLEVBQUUsQ0FBQyxDQUFELEVBQUksSUFBSixFQUFVLENBQUMsQ0FBWCxFQUFjLElBQWQsRUFBb0IsSUFBcEIsRUFBMEIsTUFBMUIsRUFBa0MsSUFBbEMsRUFBd0MsQ0FBQyxDQUF6QyxDQUpFO0FBS05FLE1BQUFBLEdBQUcsRUFBRTtBQUNEZixRQUFBQSxRQUFRLEVBQUUsVUFEVDtBQUVESyxRQUFBQSxNQUFNLEVBQUUsQ0FBQyxJQUFEO0FBRlA7QUFMQztBQXpDSDtBQVpFLENBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vY29tcG9uZW50cy9iYXNpYy1pbmZvcm1hdGlvbi9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCB7IHVzZUZvcm0gfSBmcm9tIFwicmVhY3QtaG9vay1mb3JtXCI7XHJcbmltcG9ydCB7IHVzZVN0YXRlIH0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgTGluayBmcm9tICduZXh0L2xpbmsnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQmFzaWNJbmZvcm1hdGlvbihwcm9wcykge1xyXG4vLyAgIGNvbnN0IHsgb25TdWJtaXQgfSA9IHByb3BzO1xyXG4gIGNvbnN0IHsgcmVnaXN0ZXIsIGhhbmRsZVN1Ym1pdCwgZm9ybVN0YXRlOiB7IGVycm9ycyB9IH0gPSB1c2VGb3JtKCk7XHJcbiAgY29uc3QgWyBzaG93UmVndWxlciwgc2V0U2hvd1JlZ3VsZXIgXSA9IHVzZVN0YXRlKGZhbHNlKVxyXG4gIGNvbnN0IFsgc2hvd1N5YXJpYWgsIHNldFNob3dTeWFyaWFoIF0gPSB1c2VTdGF0ZShmYWxzZSlcclxuICBjb25zdCBbIG5hc2FiYWhCcmksIHNldE5hc2FiYWhCcmkgXSA9IHVzZVN0YXRlKGZhbHNlKVxyXG4gIGNvbnN0IFsgc2hvd090cCwgc2V0U2hvd090cCBdID0gdXNlU3RhdGUoZmFsc2UpXHJcbiAgY29uc3QgWyBzaG93Tm90QnJpLCBzZXRTaG93Tm90QnJpIF0gPSB1c2VTdGF0ZShmYWxzZSlcclxuXHJcbiAgY29uc3Qgb25TdWJtaXQgPSBkYXRhID0+IHN1Ym1pdERhdGEoZGF0YSk7XHJcbiAgXHJcbiAgY29uc3QgaGFuZGxlQ2hhbmdlVHlwZVJlayA9IChlKSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGUudGFyZ2V0LnZhbHVlKVxyXG4gICAgICAgIGlmKGUudGFyZ2V0LnZhbHVlID09PSAnUmVndWxlcicpe1xyXG4gICAgICAgICAgICBzZXRTaG93UmVndWxlcih0cnVlKTtcclxuICAgICAgICAgICAgc2V0U2hvd1N5YXJpYWgoZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSBpZihlLnRhcmdldC52YWx1ZSA9PT0gJ1N5YXJpYWgnKSB7XHJcbiAgICAgICAgICAgIHNldFNob3dSZWd1bGVyKGZhbHNlKTtcclxuICAgICAgICAgICAgc2V0U2hvd1N5YXJpYWgodHJ1ZSk7XHJcbiAgICAgICAgICAgIHNldE5hc2FiYWhCcmkoZmFsc2UpO1xyXG4gICAgICAgICAgICBzZXRTaG93T3RwKGZhbHNlKTtcclxuICAgICAgICAgICAgc2V0U2hvd05vdEJyaShmYWxzZSlcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZXRTaG93UmVndWxlcihmYWxzZSk7XHJcbiAgICAgICAgICAgIHNldFNob3dTeWFyaWFoKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgfVxyXG5cclxuICBjb25zdCBoYW5kbGVDaGFuZ2VSZWZlcmVuc2kgPSBlID0+IHtcclxuICAgIGlmKGUudGFyZ2V0LnZhbHVlID09PSAneWEnKXtcclxuICAgICAgICBzZXROYXNhYmFoQnJpKHRydWUpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgICBzZXROYXNhYmFoQnJpKGZhbHNlKTtcclxuICAgICAgICBzZXRTaG93T3RwKGZhbHNlKTtcclxuICAgICAgICBzZXRTaG93Tm90QnJpKGZhbHNlKVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29uc3QgaGFuZGxlQ2hhbmdlU2hvd090cCA9IGUgPT4ge1xyXG4gICAgaWYoZS50YXJnZXQudmFsdWUgPT09ICd5YScpe1xyXG4gICAgICAgIHNldFNob3dPdHAodHJ1ZSk7XHJcbiAgICAgICAgc2V0U2hvd05vdEJyaShmYWxzZSlcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2V0U2hvd090cChmYWxzZSk7XHJcbiAgICAgICAgc2V0U2hvd05vdEJyaSh0cnVlKTtcclxuICAgIH0gXHJcbiAgfVxyXG5cclxuICBjb25zdCBzdWJtaXREYXRhID0gKGRhdGEpID0+IHtcclxuICAgIGNvbnNvbGUubG9nKGRhdGEsICdkYXRhcycpXHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbmV4dFN0ZXBwZXIxJywgJ3RydWUnKVxyXG4gIH1cclxuXHJcbiAgXHJcbiAgcmV0dXJuIChcclxuICAgIDxkaXYgY2xhc3NOYW1lPVwiYm9yZGVyLXNvbGlkIG14LWF1dG8gYmctd2hpdGUgb3ZlcmZsb3ctaGlkZGVuIGJvcmRlci0yIG10LTIwIHctMy81XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1kOmZsZXggYmctYmx1ZS00NTAgbS04IHJvdW5kZWQtMnhsXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLmNhcmR9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBzdHlsZT17c3R5bGVzLnRpdGxlfT5BTEFNQVQgUEVNT0hPTjwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxmb3JtIGlkPSdteS1mb3JtJyBjbGFzc05hbWU9XCJtbC04IG1yLThcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uU3VibWl0PXtoYW5kbGVTdWJtaXQob25TdWJtaXQpfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXggZmxleC13cmFwIC1teC0zIG1iLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LWZ1bGwgbWQ6dy0xLzIgcHgtMyB3LTIvNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImJsb2NrIHRyYWNraW5nLXdpZGUgdGV4dC1ncmF5LTcwMCB0ZXh0LXhzIGZvbnQtYm9sZCBtYi0yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBCcmFuY2ggKFNJRCkgPHNwYW4gc3R5bGU9e3tjb2xvcjpcInJlZFwifX0+Kjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInctZnVsbCBtZDp3LTEvMiBweC0zIHctMi80XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmVsYXRpdmVcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxzZWxlY3Qgey4uLnJlZ2lzdGVyKFwiYnJhbmNoSWRcIix7cmVxdWlyZWQ6IHRydWV9KX0gbmFtZT1cImJyYW5jaElkXCIgY2xhc3NOYW1lPVwiYm9yZGVyLXNvbGlkIGJvcmRlci1ncmF5LTUwMCBib3JkZXIgcHktMiBweC00IHctZnVsbCByb3VuZGVkIHRleHQtZ3JheS03MDBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiXCI+PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJCMDM5NFwiPkIwMzk0PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJCMDM5NlwiPkIwMzk2PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3NlbGVjdD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtlcnJvcnMuYnJhbmNoSWQgJiYgKDxkaXYgY2xhc3NOYW1lPVwibWItMyB0ZXh0LW5vcm1hbCB0ZXh0LXJlZC01MDAgXCI+e1wiRmllbGQgaXMgcmVxdWlyZWRcIn08L2Rpdj4pfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LXdyYXAgLW14LTMgbWItNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInctZnVsbCBtZDp3LTEvMiBweC0zIHctMi80XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwiYmxvY2sgdHJhY2tpbmctd2lkZSB0ZXh0LWdyYXktNzAwIHRleHQteHMgZm9udC1ib2xkIG1iLTJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFJlZmVyYWwgQ29kZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIG1kOnctMS8yIHB4LTMgdy0yLzRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxpbnB1dCBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJvcmRlci1zb2xpZCBib3JkZXItZ3JheS01MDAgYm9yZGVyIHB5LTIgcHgtNCB3LWZ1bGwgcm91bmRlZCB0ZXh0LWdyYXktNzAwXCIgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lPVwicmVmZXJhbENvZGVcIlxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1cyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5yZWdpc3RlcihcInJlZmVyYWxDb2RlXCIpfSAvPiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtlcnJvcnMucmVmZXJhbENvZGUgJiYgKDxkaXYgY2xhc3NOYW1lPVwibWItMyB0ZXh0LW5vcm1hbCB0ZXh0LXJlZC01MDAgXCI+e1wiRmllbGQgaXMgcmVxdWlyZWRcIn08L2Rpdj4pfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXggZmxleC13cmFwIC1teC0zIG1iLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LWZ1bGwgbWQ6dy0xLzIgcHgtMyB3LTIvNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImJsb2NrIHRyYWNraW5nLXdpZGUgdGV4dC1ncmF5LTcwMCB0ZXh0LXhzIGZvbnQtYm9sZCBtYi0yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBKZW5pcyByZWtlbmluZyB5YW5nIEFuZGEgaW5naW5rYW4gPyA8c3BhbiBzdHlsZT17e2NvbG9yOlwicmVkXCJ9fT4qPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIG1kOnctMS8yIHB4LTMgdy0yLzRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyZWxhdGl2ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCB7Li4ucmVnaXN0ZXIoXCJ0eXBlUmVrXCIse3JlcXVpcmVkOiB0cnVlfSl9IG5hbWU9XCJ0eXBlUmVrXCIgY2xhc3NOYW1lPVwiYm9yZGVyLXNvbGlkIGJvcmRlci1ncmF5LTUwMCBib3JkZXIgcHktMiBweC00IHctZnVsbCByb3VuZGVkIHRleHQtZ3JheS03MDBcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e2hhbmRsZUNoYW5nZVR5cGVSZWt9PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiXCI+PC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJSZWd1bGVyXCI+UmVndWxlcjwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwiU3lhcmlhaFwiPlN5YXJpYWg8L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2Vycm9ycy50eXBlUmVrICYmICg8ZGl2IGNsYXNzTmFtZT1cIm1iLTMgdGV4dC1ub3JtYWwgdGV4dC1yZWQtNTAwIFwiPntcIkZpZWxkIGlzIHJlcXVpcmVkXCJ9PC9kaXY+KX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7c2hvd1JlZ3VsZXIgPyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXggZmxleC13cmFwIC1teC0zIG1iLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LWZ1bGwgbWQ6dy0xLzIgcHgtMyB3LTIvNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImJsb2NrIHRyYWNraW5nLXdpZGUgdGV4dC1ncmF5LTcwMCB0ZXh0LXhzIGZvbnQtYm9sZCBtYi0yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBBcGFrYWggYW5kYSBtZW5kYXBhdCByZWZlcmVuc2kgZGFyaSBCUkkgPyA8c3BhbiBzdHlsZT17e2NvbG9yOlwicmVkXCJ9fT4qPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIG1kOnctMS8yIHB4LTMgdy0yLzRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyZWxhdGl2ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCB7Li4ucmVnaXN0ZXIoXCJpc1JlZmVyZW5zaUJyaVwiLHtyZXF1aXJlZDogdHJ1ZX0pfSBuYW1lPVwiaXNSZWZlcmVuc2lCcmlcIiBjbGFzc05hbWU9XCJib3JkZXItc29saWQgYm9yZGVyLWdyYXktNTAwIGJvcmRlciBweS0yIHB4LTQgdy1mdWxsIHJvdW5kZWQgdGV4dC1ncmF5LTcwMFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17aGFuZGxlQ2hhbmdlUmVmZXJlbnNpfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJcIj48L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJ5YVwiPllBPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwidGlkYWtcIj5USURBSzwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZXJyb3JzLmlzUmVmZXJlbnNpQnJpICYmICg8ZGl2IGNsYXNzTmFtZT1cIm1iLTMgdGV4dC1ub3JtYWwgdGV4dC1yZWQtNTAwIFwiPntcIkZpZWxkIGlzIHJlcXVpcmVkXCJ9PC9kaXY+KX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiA6IG51bGx9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge25hc2FiYWhCcmkgPyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXggZmxleC13cmFwIC1teC0zIG1iLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LWZ1bGwgbWQ6dy0xLzIgcHgtMyB3LTIvNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImJsb2NrIHRyYWNraW5nLXdpZGUgdGV4dC1ncmF5LTcwMCB0ZXh0LXhzIGZvbnQtYm9sZCBtYi0yXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBBcGFrYWggc2FhdCBpbmkgQW5kYSBzZWJhZ2FpIG5hc2FiYWggQlJJPyA8c3BhbiBzdHlsZT17e2NvbG9yOlwicmVkXCJ9fT4qPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIG1kOnctMS8yIHB4LTMgdy0yLzRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyZWxhdGl2ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPHNlbGVjdCB7Li4ucmVnaXN0ZXIoXCJpc05hc2FiYWhCcmlcIix7cmVxdWlyZWQ6IHRydWV9KX0gbmFtZT1cImlzTmFzYWJhaEJyaVwiIGNsYXNzTmFtZT1cImJvcmRlci1zb2xpZCBib3JkZXItZ3JheS01MDAgYm9yZGVyIHB5LTIgcHgtNCB3LWZ1bGwgcm91bmRlZCB0ZXh0LWdyYXktNzAwXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtoYW5kbGVDaGFuZ2VTaG93T3RwfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJcIj48L29wdGlvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24gdmFsdWU9XCJ5YVwiPllBPC9vcHRpb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uIHZhbHVlPVwidGlkYWtcIj5USURBSzwvb3B0aW9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9zZWxlY3Q+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZXJyb3JzLmlzTmFzYWJhaEJyaSAmJiAoPGRpdiBjbGFzc05hbWU9XCJtYi0zIHRleHQtbm9ybWFsIHRleHQtcmVkLTUwMCBcIj57XCJGaWVsZCBpcyByZXF1aXJlZFwifTwvZGl2Pil9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gOiBudWxsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LXdyYXAgLW14LTMgbWItMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzaG93U3lhcmlhaCA/IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1kOmZsZXggcm91bmRlZC0yeGwgaXRlbXMtY2VudGVyIHB4LTE2XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuY2FyZH0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cCBzdHlsZT17e3RleHRBbGlnbjpcImNlbnRlclwifX0gY2xhc3NOYW1lPVwiYmxvY2sgdHJhY2tpbmctd2lkZSB0ZXh0LWdyYXktNzAwIHRleHQteHMgZm9udC1ib2xkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgRFNOLU1VSSBOby4gODAvRFNOLU1VSS9JSUkvMjAxMSAtIFBlbmVyYXBhbiBQcmluc2lwIFN5YXJpYWggUGVyZGFnYW5nYW4gRWZlayBFa3VpdGFzLCBzaWxhaGthbiBrbGlrIDxMaW5rIGhyZWY9Jy9hdXRoL2xvZ2luJz48c3BhbiBzdHlsZT17e2NvbG9yOlwiYmx1ZVwifX0+ZGlzaW5pPC9zcGFuPjwvTGluaz4gZGFuIGZhdHdhIDxMaW5rIGhyZWY9Jy9hdXRoL2xvZ2luJz48c3BhbiBzdHlsZT17e2NvbG9yOlwiYmx1ZVwifX0+bGFpbm55YS48L3NwYW4+PC9MaW5rPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gOiBudWxsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzaG93Tm90QnJpID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWQ6ZmxleCByb3VuZGVkLTJ4bCBpdGVtcy1jZW50ZXIgcHgtMTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5jYXJkfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIHN0eWxlPXt7dGV4dEFsaWduOlwiY2VudGVyXCJ9fSBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJsb2NrIHRyYWNraW5nLXdpZGUgdGV4dC1ncmF5LTcwMCB0ZXh0LXhzIGZvbnQtYm9sZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFVudHVrIG1lbGFuanV0a2FuIHByb3NlcyBwZW1idWt1YWFuIHJla2VuaW5nIEJSSSBEYW5hcmVrc2EsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGthbWkgc2FyYW5rYW4gQW5kYSB1bnR1ayBtZW1idWthIHJla2VuaW5nIEJhbmsgQlJJIG1lbGFsdWkgbGluayBiZXJpa3V0IGluaSA6IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMaW5rIGhyZWY9XCIvYXV0aC9sb2dpblwiPjxzcGFuIHN0eWxlPXt7Y29sb3I6XCJibHVlXCJ9fT5idWthcmVrZW5pbmcuYnJpLmNvLmlkPC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvTGluaz48L3A+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IDogbnVsbH0gIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LXdyYXAgLW14LTMgbWItMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzaG93T3RwID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWQ6ZmxleCByb3VuZGVkLTJ4bCBpdGVtcy1jZW50ZXIgcHgtMTZcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5jYXJkfT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxwIHN0eWxlPXt7dGV4dEFsaWduOlwiY2VudGVyXCJ9fSBjbGFzc05hbWU9XCJibG9jayB0cmFja2luZy13aWRlIHRleHQtZ3JheS03MDAgdGV4dC14cyBmb250LWJvbGRcIj5QYXN0aWthbiBOb21vciBIYW5kcGhvbmUgZGFuIE5vbW9yIFJla2VuaW5nIEFuZGEgc3VkYWggdGVyZGFmdGFyIGRpIEJSSS5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5vbW9yIEhhbnBob25lIGRhbiBOb21vciBSZWtlbmluZyBCUkkgYWthbiBrYW1pIGtpcmlta2FuIGtlIEJSSSB1bnR1ayBwcm9zZXMgdmVyaWZpa2FzaS48L3A+PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiA6IG51bGx9IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzaG93T3RwID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGZsZXgtd3JhcCAtbXgtMyBtYi02XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIG1kOnctMS8yIHB4LTMgdy0yLzRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJibG9jayB0cmFja2luZy13aWRlIHRleHQtZ3JheS03MDAgdGV4dC14cyBmb250LWJvbGQgbWItMlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgTm8gSGFuZHBob25lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LWZ1bGwgbWQ6dy0xLzIgcHgtMyB3LTIvNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8aW5wdXQgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJib3JkZXItc29saWQgYm9yZGVyLWdyYXktNTAwIGJvcmRlciBweS0yIHB4LTQgdy1mdWxsIHJvdW5kZWQgdGV4dC1ncmF5LTcwMFwiIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZT1cIm5vSHBcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1cyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHsuLi5yZWdpc3RlcihcIm5vSHBcIix7cmVxdWlyZWQ6IHRydWUsIHBhdHRlcm46IC9eWytdKlsoXXswLDF9WzAtOV17MSw0fVspXXswLDF9Wy1cXHNcXC4vMC05XSokL30pfSAvPiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtjb25zb2xlLmxvZyhlcnJvcnMubm9IcCl9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7ZXJyb3JzLm5vSHAgJiYgZXJyb3JzLm5vSHAudHlwZT09PVwicmVxdWlyZWRcIiAmJiAoPGRpdiBjbGFzc05hbWU9XCJtYi0zIHRleHQtbm9ybWFsIHRleHQtcmVkLTUwMFwiPntcIkZpZWxkIGlzIHJlcXVpcmVkXCJ9PC9kaXY+KX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtlcnJvcnMubm9IcCAmJiBlcnJvcnMubm9IcC50eXBlPT09XCJwYXR0ZXJuXCIgJiYgKDxkaXYgY2xhc3NOYW1lPVwibWItMyB0ZXh0LW5vcm1hbCB0ZXh0LXJlZC01MDBcIj57XCJGaWVsZCBpcyBOdW1iZXJcIn08L2Rpdj4pfSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IDogbnVsbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7c2hvd090cCA/IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LXdyYXAgLW14LTMgbWItNlwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInctZnVsbCBtZDp3LTEvMiBweC0zIHctMi80XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwiYmxvY2sgdHJhY2tpbmctd2lkZSB0ZXh0LWdyYXktNzAwIHRleHQteHMgZm9udC1ib2xkIG1iLTJcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIE5vIFJla2VuaW5nIEJSSVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIG1kOnctMS8yIHB4LTMgdy0yLzRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGlucHV0IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYm9yZGVyLXNvbGlkIGJvcmRlci1ncmF5LTUwMCBib3JkZXIgcHktMiBweC00IHctZnVsbCByb3VuZGVkIHRleHQtZ3JheS03MDBcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU9XCJOb1Jla0JyaVwiIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgey4uLnJlZ2lzdGVyKFwiTm9SZWtCcmlcIix7cmVxdWlyZWQ6IHRydWUsIHBhdHRlcm46IC9eWytdKlsoXXswLDF9WzAtOV17MSw0fVspXXswLDF9Wy1cXHNcXC4vMC05XSokL30pfSAvPiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtlcnJvcnMuTm9SZWtCcmkgJiYgZXJyb3JzLk5vUmVrQnJpLnR5cGU9PT1cInJlcXVpcmVkXCIgJiYgKDxkaXYgY2xhc3NOYW1lPVwibWItMyB0ZXh0LW5vcm1hbCB0ZXh0LXJlZC01MDBcIj57XCJGaWVsZCBpcyByZXF1aXJlZFwifTwvZGl2Pil9IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge2Vycm9ycy5Ob1Jla0JyaSAmJiBlcnJvcnMuTm9SZWtCcmkudHlwZT09PVwicGF0dGVyblwiICYmICg8ZGl2IGNsYXNzTmFtZT1cIm1iLTMgdGV4dC1ub3JtYWwgdGV4dC1yZWQtNTAwIFwiPntcIkZpZWxkIGlzIE51bWJlclwifTwvZGl2Pil9ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gOiBudWxsfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtzaG93T3RwID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgganVzdGlmeS1jZW50ZXIgbWItM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJiZy15ZWxsb3ctNTAwIHRleHQtd2hpdGUgZm9udC1ib2xkIHB5LTIgcHgtNCByb3VuZGVkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCIgb25DbGljaz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBLaXJpbSBPVFBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IDogbnVsbH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgKTtcclxufVxyXG5cclxuY29uc3Qgc3R5bGVzID0ge1xyXG4gIGNvbnRhaW5lcjoge1xyXG4gICAgICBtaW5IZWlnaHQ6ICdpbmhlcml0JyxcclxuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxyXG4gICAgICBmbGV4RGlyZWN0aW9uOiAnY29sdW1uJyxcclxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxyXG4gIH0sXHJcbiAgY2FyZDoge1xyXG4gICAgICBtYXJnaW46IDI1XHJcbiAgfSxcclxuICB0aXRsZToge1xyXG4gICAgICBjb2xvcjogXCIjRkZGRkZGXCJcclxuICB9LCBiYW5uZXI6IHtcclxuICAgICAgcHQ6IFsnMTQwcHgnLCAnMTQ1cHgnLCAnMTU1cHgnLCAnMTcwcHgnLCBudWxsLCBudWxsLCAnMTgwcHgnLCAnMjE1cHgnXSxcclxuICAgICAgcGI6IFsyLCBudWxsLCAwLCBudWxsLCAyLCAwLCBudWxsLCA1XSxcclxuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXHJcbiAgICAgIHpJbmRleDogMixcclxuICAgICAgJyY6OmJlZm9yZSc6IHtcclxuICAgICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxyXG4gICAgICAgICAgY29udGVudDogJ1wiXCInLFxyXG4gICAgICAgICAgYm90dG9tOiA2LFxyXG4gICAgICAgICAgbGVmdDogMCxcclxuICAgICAgICAgIGhlaWdodDogJzEwMCUnLFxyXG4gICAgICAgICAgd2lkdGg6ICcxMDAlJyxcclxuICAgICAgICAgIHpJbmRleDogLTEsXHJcbiAgICAgICAgICBiYWNrZ3JvdW5kUmVwZWF0OiBgbm8tcmVwZWF0YCxcclxuICAgICAgICAgIGJhY2tncm91bmRQb3NpdGlvbjogJ2JvdHRvbSBsZWZ0JyxcclxuICAgICAgICAgIGJhY2tncm91bmRTaXplOiAnMzYlJyxcclxuICAgICAgfSxcclxuICAgICAgJyY6OmFmdGVyJzoge1xyXG4gICAgICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXHJcbiAgICAgICAgICBjb250ZW50OiAnXCJcIicsXHJcbiAgICAgICAgICBib3R0b206ICc0MHB4JyxcclxuICAgICAgICAgIHJpZ2h0OiAwLFxyXG4gICAgICAgICAgaGVpZ2h0OiAnMTAwJScsXHJcbiAgICAgICAgICB3aWR0aDogJzEwMCUnLFxyXG4gICAgICAgICAgekluZGV4OiAtMSxcclxuICAgICAgICAgIGJhY2tncm91bmRSZXBlYXQ6IGBuby1yZXBlYXRgLFxyXG4gICAgICAgICAgYmFja2dyb3VuZFBvc2l0aW9uOiAnYm90dG9tIHJpZ2h0JyxcclxuICAgICAgICAgIGJhY2tncm91bmRTaXplOiAnMzIlJyxcclxuICAgICAgfSxcclxuICAgICAgY29udGFpbmVyOiB7XHJcbiAgICAgICAgICBtaW5IZWlnaHQ6ICdpbmhlcml0JyxcclxuICAgICAgICAgIGRpc3BsYXk6ICdmbGV4JyxcclxuICAgICAgICAgIGZsZXhEaXJlY3Rpb246ICdjb2x1bW4nLFxyXG4gICAgICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxyXG4gICAgICB9LFxyXG4gICAgICBjb250ZW50Qm94OiB7XHJcbiAgICAgICAgICB3aWR0aDogWycxMDAlJywgJzkwJScsICc1MzVweCcsIG51bGwsICc1NyUnLCAnNjAlJywgJzY4JScsICc2MCUnXSxcclxuICAgICAgICAgIG14OiAnYXV0bycsXHJcbiAgICAgICAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxyXG4gICAgICAgICAgbWI6IFsnNDBweCcsIG51bGwsIG51bGwsIG51bGwsIG51bGwsIDddLFxyXG4gICAgICB9LFxyXG4gICAgICBpbWFnZUJveDoge1xyXG4gICAgICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxyXG4gICAgICAgICAgdGV4dEFsaWduOiAnY2VudGVyJyxcclxuICAgICAgICAgIGRpc3BsYXk6ICdpbmxpbmUtZmxleCcsXHJcbiAgICAgICAgICBtYjogWzAsIG51bGwsIC02LCBudWxsLCBudWxsLCAnMTBweCcsIG51bGwsIC0zXSxcclxuICAgICAgICAgIGltZzoge1xyXG4gICAgICAgICAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxyXG4gICAgICAgICAgICAgIGhlaWdodDogWzEwMDBdLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgfSxcclxuICB9LFxyXG59Il0sIm5hbWVzIjpbIlJlYWN0IiwidXNlRm9ybSIsInVzZVN0YXRlIiwiTGluayIsIkJhc2ljSW5mb3JtYXRpb24iLCJwcm9wcyIsInJlZ2lzdGVyIiwiaGFuZGxlU3VibWl0IiwiZXJyb3JzIiwiZm9ybVN0YXRlIiwic2hvd1JlZ3VsZXIiLCJzZXRTaG93UmVndWxlciIsInNob3dTeWFyaWFoIiwic2V0U2hvd1N5YXJpYWgiLCJuYXNhYmFoQnJpIiwic2V0TmFzYWJhaEJyaSIsInNob3dPdHAiLCJzZXRTaG93T3RwIiwic2hvd05vdEJyaSIsInNldFNob3dOb3RCcmkiLCJvblN1Ym1pdCIsImRhdGEiLCJzdWJtaXREYXRhIiwiaGFuZGxlQ2hhbmdlVHlwZVJlayIsImUiLCJjb25zb2xlIiwibG9nIiwidGFyZ2V0IiwidmFsdWUiLCJoYW5kbGVDaGFuZ2VSZWZlcmVuc2kiLCJoYW5kbGVDaGFuZ2VTaG93T3RwIiwibG9jYWxTdG9yYWdlIiwic2V0SXRlbSIsInN0eWxlcyIsImNhcmQiLCJ0aXRsZSIsImNvbG9yIiwicmVxdWlyZWQiLCJicmFuY2hJZCIsInJlZmVyYWxDb2RlIiwidHlwZVJlayIsImlzUmVmZXJlbnNpQnJpIiwiaXNOYXNhYmFoQnJpIiwidGV4dEFsaWduIiwicGF0dGVybiIsIm5vSHAiLCJ0eXBlIiwiTm9SZWtCcmkiLCJjb250YWluZXIiLCJtaW5IZWlnaHQiLCJkaXNwbGF5IiwiZmxleERpcmVjdGlvbiIsImp1c3RpZnlDb250ZW50IiwibWFyZ2luIiwiYmFubmVyIiwicHQiLCJwYiIsInBvc2l0aW9uIiwiekluZGV4IiwiY29udGVudCIsImJvdHRvbSIsImxlZnQiLCJoZWlnaHQiLCJ3aWR0aCIsImJhY2tncm91bmRSZXBlYXQiLCJiYWNrZ3JvdW5kUG9zaXRpb24iLCJiYWNrZ3JvdW5kU2l6ZSIsInJpZ2h0IiwiY29udGVudEJveCIsIm14IiwibWIiLCJpbWFnZUJveCIsImltZyJdLCJzb3VyY2VSb290IjoiIn0=