"use strict";
self["webpackHotUpdate_N_E"]("pages/auth/login",{

/***/ "./pages/auth/login.js":
/*!*****************************!*\
  !*** ./pages/auth/login.js ***!
  \*****************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ SignInSide; }
/* harmony export */ });
/* harmony import */ var C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js");
/* harmony import */ var C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty */ "./node_modules/next/node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./node_modules/next/node_modules/@babel/runtime/regenerator */ "./node_modules/next/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _mui_material_CssBaseline__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @mui/material/CssBaseline */ "./node_modules/@mui/material/CssBaseline/index.js");
/* harmony import */ var _mui_material_Link__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @mui/material/Link */ "./node_modules/@mui/material/Link/index.js");
/* harmony import */ var _mui_material_Paper__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @mui/material/Paper */ "./node_modules/@mui/material/Paper/index.js");
/* harmony import */ var _mui_material_Box__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @mui/material/Box */ "./node_modules/@mui/material/Box/index.js");
/* harmony import */ var _mui_material_Grid__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @mui/material/Grid */ "./node_modules/@mui/material/Grid/index.js");
/* harmony import */ var _mui_material_Typography__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @mui/material/Typography */ "./node_modules/@mui/material/Typography/index.js");
/* harmony import */ var _mui_material_styles__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @mui/material/styles */ "./node_modules/@mui/material/styles/index.js");
/* harmony import */ var _images_logo_danareksa_png__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../images/logo-danareksa.png */ "./images/logo-danareksa.png");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/image */ "./node_modules/next/image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var react_hook_form__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-hook-form */ "./node_modules/react-hook-form/dist/index.esm.js");
/* harmony import */ var _lib_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../lib/service */ "./lib/service.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__);
/* module decorator */ module = __webpack_require__.hmd(module);



var _jsxFileName = "C:\\NEXTJS\\eform-bri\\pages\\auth\\login.js",
    _s = $RefreshSig$();



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0,C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_1__.default)(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }























function Copyright(props) {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_Typography__WEBPACK_IMPORTED_MODULE_10__.default, _objectSpread(_objectSpread({
    variant: "body2",
    color: "text.secondary",
    align: "center"
  }, props), {}, {
    children: ['Copyright © ', /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_Link__WEBPACK_IMPORTED_MODULE_11__.default, {
      color: "inherit",
      href: "https://material-ui.com/",
      children: "Your Website"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this), ' ', new Date().getFullYear(), '.']
  }), void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 26,
    columnNumber: 5
  }, this);
}

_c = Copyright;
var theme = (0,_mui_material_styles__WEBPACK_IMPORTED_MODULE_12__.createTheme)();
function SignInSide() {
  _s();

  var router = (0,next_router__WEBPACK_IMPORTED_MODULE_8__.useRouter)();

  var _useForm = (0,react_hook_form__WEBPACK_IMPORTED_MODULE_6__.useForm)(),
      register = _useForm.register,
      handleSubmit = _useForm.handleSubmit;

  var onSubmit = function onSubmit(data) {
    return submitData(data);
  };

  var submitData = /*#__PURE__*/function () {
    var _ref = (0,C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_0__.default)( /*#__PURE__*/C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default().mark(function _callee(data) {
      var authResponse;
      return C_NEXTJS_eform_bri_node_modules_next_node_modules_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              console.log(data, 'datas');
              _context.next = 3;
              return _lib_service__WEBPACK_IMPORTED_MODULE_7__.auth.login(data.email, data.password);

            case 3:
              authResponse = _context.sent;
              console.log(authResponse);

              if (!authResponse.error) {
                router.push('/eform');
              } else {
                router.push('/login');
              }

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function submitData(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_styles__WEBPACK_IMPORTED_MODULE_12__.ThemeProvider, {
    theme: theme,
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_Grid__WEBPACK_IMPORTED_MODULE_13__.default, {
      container: true,
      component: "main",
      sx: {
        height: '100vh'
      },
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_CssBaseline__WEBPACK_IMPORTED_MODULE_14__.default, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 59,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_Grid__WEBPACK_IMPORTED_MODULE_13__.default, {
        item: true,
        xs: false,
        sm: 4,
        md: 7,
        sx: {
          backgroundImage: 'url(https://source.unsplash.com/random)',
          backgroundRepeat: 'no-repeat',
          backgroundColor: function backgroundColor(t) {
            return t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900];
          },
          backgroundSize: 'cover',
          backgroundPosition: 'center'
        }
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 60,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_Grid__WEBPACK_IMPORTED_MODULE_13__.default, {
        item: true,
        xs: 16,
        sm: 8,
        md: 5,
        component: _mui_material_Paper__WEBPACK_IMPORTED_MODULE_15__.default,
        elevation: 6,
        square: true,
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)(_mui_material_Box__WEBPACK_IMPORTED_MODULE_16__.default, {
          sx: {
            my: 10,
            mx: 10,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center'
          },
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
            src: _images_logo_danareksa_png__WEBPACK_IMPORTED_MODULE_4__.default
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 84,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("br", {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 85,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
            className: "w-full max-w-xs",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("form", {
              onSubmit: handleSubmit(onSubmit),
              className: "bg-gray-200 shadow-md rounded px-8 pt-6 pb-8 mb-4",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
                className: "mb-4",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("label", {
                  className: "block text-gray-700 text-sm font-bold mb-2",
                  "for": "username",
                  children: "Email"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 89,
                  columnNumber: 21
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("input", _objectSpread({
                  className: "shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline",
                  id: "email",
                  type: "text"
                }, register("email")), void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 92,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 88,
                columnNumber: 21
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
                className: "mb-6",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("label", {
                  className: "block text-gray-700 text-sm font-bold mb-2",
                  "for": "password",
                  children: "Password"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 95,
                  columnNumber: 21
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("input", _objectSpread({
                  className: "shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline",
                  id: "password",
                  type: "password",
                  placeholder: "Masukan Password"
                }, register("password")), void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 98,
                  columnNumber: 21
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("p", {
                  className: "text-xs italic",
                  children: "Please choose a password."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 99,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 94,
                columnNumber: 21
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
                className: "flex items-center justify-between",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("button", {
                  className: "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline",
                  type: "submit",
                  children: "MASUK"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 102,
                  columnNumber: 21
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("a", {
                  className: "inline-block align-baseline font-bold text-sm text-blue-500 hover:text-blue-800",
                  href: "#",
                  children: "Belum Memiliki Akun ?"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 105,
                  columnNumber: 21
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 101,
                columnNumber: 21
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 87,
              columnNumber: 17
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("p", {
              className: "text-center text-gray-500 text-xs",
              children: "\xA92020 Acme Corp. All rights reserved."
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 110,
              columnNumber: 17
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 86,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("br", {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 114,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("form", {
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
              className: "flex flex-wrap -mx-3 mb-3",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
                className: "w-full px-3",
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("button", {
                  className: "btn px-0 text-white bg-gray-900 hover:bg-gray-800 w-full rounded flex items-center",
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("svg", {
                    className: "w-4 h-4 fill-current text-white opacity-75 flex-shrink-0 mx-4",
                    viewBox: "0 0 16 16",
                    xmlns: "http://www.w3.org/2000/svg",
                    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("path", {
                      d: "M7.95 0C3.578 0 0 3.578 0 7.95c0 3.479 2.286 6.46 5.466 7.553.397.1.497-.199.497-.397v-1.392c-2.187.497-2.683-.993-2.683-.993-.398-.895-.895-1.193-.895-1.193-.696-.497.1-.497.1-.497.795.1 1.192.795 1.192.795.696 1.292 1.888.895 2.286.696.1-.497.298-.895.497-1.093-1.79-.2-3.578-.895-3.578-3.975 0-.895.298-1.59.795-2.087-.1-.2-.397-.994.1-2.087 0 0 .695-.2 2.186.795a6.408 6.408 0 011.987-.299c.696 0 1.392.1 1.988.299 1.49-.994 2.186-.795 2.186-.795.398 1.093.199 1.888.1 2.087.496.596.795 1.291.795 2.087 0 3.08-1.889 3.677-3.677 3.875.298.398.596.895.596 1.59v2.187c0 .198.1.497.596.397C13.714 14.41 16 11.43 16 7.95 15.9 3.578 12.323 0 7.95 0z"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 120,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 119,
                    columnNumber: 25
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("span", {
                    className: "flex-auto pl-20 pr-10 -ml-20",
                    children: "Continue with GitHub"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 122,
                    columnNumber: 25
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 118,
                  columnNumber: 23
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 117,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 116,
              columnNumber: 19
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
              className: "flex flex-wrap -mx-3",
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("div", {
                className: "w-full px-3",
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("button", {
                  className: "btn px-0 text-white bg-red-600 hover:bg-red-700 w-full rounded flex items-center",
                  children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("svg", {
                    className: "w-4 h-4 fill-current text-white opacity-75 flex-shrink-0 mx-4",
                    viewBox: "0 0 16 16",
                    xmlns: "http://www.w3.org/2000/svg",
                    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("path", {
                      d: "M7.9 7v2.4H12c-.2 1-1.2 3-4 3-2.4 0-4.3-2-4.3-4.4 0-2.4 2-4.4 4.3-4.4 1.4 0 2.3.6 2.8 1.1l1.9-1.8C11.5 1.7 9.9 1 8 1 4.1 1 1 4.1 1 8s3.1 7 7 7c4 0 6.7-2.8 6.7-6.8 0-.5 0-.8-.1-1.2H7.9z"
                    }, void 0, false, {
                      fileName: _jsxFileName,
                      lineNumber: 130,
                      columnNumber: 27
                    }, this)
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 129,
                    columnNumber: 25
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxDEV)("span", {
                    className: "flex-auto pl-16 pr-8 -ml-16",
                    children: "Continue with Google"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 132,
                    columnNumber: 25
                  }, this)]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 128,
                  columnNumber: 23
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 127,
                columnNumber: 21
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 126,
              columnNumber: 19
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 115,
            columnNumber: 14
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 75,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 74,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 58,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 57,
    columnNumber: 5
  }, this);
}

_s(SignInSide, "zvFWp0J8R9+bTSWHt4Zj8n5h0cs=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_8__.useRouter, react_hook_form__WEBPACK_IMPORTED_MODULE_6__.useForm];
});

_c2 = SignInSide;

var _c, _c2;

$RefreshReg$(_c, "Copyright");
$RefreshReg$(_c2, "SignInSide");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvYXV0aC9sb2dpbi41MDI5OTcxODE5YjUyY2E0OTU3Yi5ob3QtdXBkYXRlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUlBLFNBQVNxQixTQUFULENBQW1CQyxLQUFuQixFQUEwQjtBQUN4QixzQkFDRSw4REFBQyw4REFBRDtBQUFZLFdBQU8sRUFBQyxPQUFwQjtBQUE0QixTQUFLLEVBQUMsZ0JBQWxDO0FBQW1ELFNBQUssRUFBQztBQUF6RCxLQUFzRUEsS0FBdEU7QUFBQSxlQUNHLGNBREgsZUFFRSw4REFBQyx3REFBRDtBQUFNLFdBQUssRUFBQyxTQUFaO0FBQXNCLFVBQUksRUFBQywwQkFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGRixFQUlVLEdBSlYsRUFLRyxJQUFJQyxJQUFKLEdBQVdDLFdBQVgsRUFMSCxFQU1HLEdBTkg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFVRDs7S0FYUUg7QUFhVCxJQUFNSSxLQUFLLEdBQUdaLGtFQUFXLEVBQXpCO0FBRWUsU0FBU2EsVUFBVCxHQUFzQjtBQUFBOztBQUduQyxNQUFNQyxNQUFNLEdBQUdQLHNEQUFTLEVBQXhCOztBQUNBLGlCQUFrQ0gsd0RBQU8sRUFBekM7QUFBQSxNQUFPVyxRQUFQLFlBQU9BLFFBQVA7QUFBQSxNQUFpQkMsWUFBakIsWUFBaUJBLFlBQWpCOztBQUNBLE1BQU1DLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQUFDLElBQUk7QUFBQSxXQUFJQyxVQUFVLENBQUNELElBQUQsQ0FBZDtBQUFBLEdBQXJCOztBQUNBLE1BQU1DLFVBQVU7QUFBQSwyUkFBRyxpQkFBT0QsSUFBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDZkUsY0FBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVlILElBQVosRUFBa0IsT0FBbEI7QUFEZTtBQUFBLHFCQUVRWixvREFBQSxDQUFXWSxJQUFJLENBQUNLLEtBQWhCLEVBQXNCTCxJQUFJLENBQUNNLFFBQTNCLENBRlI7O0FBQUE7QUFFYkMsY0FBQUEsWUFGYTtBQUdqQkwsY0FBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVlJLFlBQVo7O0FBQ0csa0JBQUksQ0FBQ0EsWUFBWSxDQUFDQyxLQUFsQixFQUF5QjtBQUMxQlosZ0JBQUFBLE1BQU0sQ0FBQ2EsSUFBUCxDQUFZLFFBQVo7QUFDRCxlQUZFLE1BRUk7QUFDSGIsZ0JBQUFBLE1BQU0sQ0FBQ2EsSUFBUCxDQUFZLFFBQVo7QUFDSDs7QUFSZ0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBSDs7QUFBQSxvQkFBVlIsVUFBVTtBQUFBO0FBQUE7QUFBQSxLQUFoQjs7QUFXQSxzQkFDRSw4REFBQyxnRUFBRDtBQUFlLFNBQUssRUFBRVAsS0FBdEI7QUFBQSwyQkFDRSw4REFBQyx3REFBRDtBQUFNLGVBQVMsTUFBZjtBQUFnQixlQUFTLEVBQUMsTUFBMUI7QUFBaUMsUUFBRSxFQUFFO0FBQUVnQixRQUFBQSxNQUFNLEVBQUU7QUFBVixPQUFyQztBQUFBLDhCQUNFLDhEQUFDLCtEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQUVFLDhEQUFDLHdEQUFEO0FBQ0UsWUFBSSxNQUROO0FBRUUsVUFBRSxFQUFFLEtBRk47QUFHRSxVQUFFLEVBQUUsQ0FITjtBQUlFLFVBQUUsRUFBRSxDQUpOO0FBS0UsVUFBRSxFQUFFO0FBQ0ZDLFVBQUFBLGVBQWUsRUFBRSx5Q0FEZjtBQUVGQyxVQUFBQSxnQkFBZ0IsRUFBRSxXQUZoQjtBQUdGQyxVQUFBQSxlQUFlLEVBQUUseUJBQUNDLENBQUQ7QUFBQSxtQkFDZkEsQ0FBQyxDQUFDQyxPQUFGLENBQVVDLElBQVYsS0FBbUIsT0FBbkIsR0FBNkJGLENBQUMsQ0FBQ0MsT0FBRixDQUFVRSxJQUFWLENBQWUsRUFBZixDQUE3QixHQUFrREgsQ0FBQyxDQUFDQyxPQUFGLENBQVVFLElBQVYsQ0FBZSxHQUFmLENBRG5DO0FBQUEsV0FIZjtBQUtGQyxVQUFBQSxjQUFjLEVBQUUsT0FMZDtBQU1GQyxVQUFBQSxrQkFBa0IsRUFBRTtBQU5sQjtBQUxOO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGRixlQWdCRSw4REFBQyx3REFBRDtBQUFNLFlBQUksTUFBVjtBQUFXLFVBQUUsRUFBRSxFQUFmO0FBQW1CLFVBQUUsRUFBRSxDQUF2QjtBQUEwQixVQUFFLEVBQUUsQ0FBOUI7QUFBaUMsaUJBQVMsRUFBRTFDLHlEQUE1QztBQUFtRCxpQkFBUyxFQUFFLENBQTlEO0FBQWlFLGNBQU0sTUFBdkU7QUFBQSwrQkFDRSw4REFBQyx1REFBRDtBQUNFLFlBQUUsRUFBRTtBQUNGMkMsWUFBQUEsRUFBRSxFQUFFLEVBREY7QUFFRkMsWUFBQUEsRUFBRSxFQUFFLEVBRkY7QUFHRkMsWUFBQUEsT0FBTyxFQUFFLE1BSFA7QUFJRkMsWUFBQUEsYUFBYSxFQUFFLFFBSmI7QUFLRkMsWUFBQUEsVUFBVSxFQUFFO0FBTFYsV0FETjtBQUFBLGtDQVNFLDhEQUFDLG1EQUFEO0FBQU8sZUFBRyxFQUFFeEMsK0RBQWFBO0FBQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBVEYsZUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQVZGLGVBV0U7QUFBSyxxQkFBUyxFQUFDLGlCQUFmO0FBQUEsb0NBQ0k7QUFBTSxzQkFBUSxFQUFFYyxZQUFZLENBQUNDLFFBQUQsQ0FBNUI7QUFBd0MsdUJBQVMsRUFBQyxtREFBbEQ7QUFBQSxzQ0FDSTtBQUFLLHlCQUFTLEVBQUMsTUFBZjtBQUFBLHdDQUNBO0FBQU8sMkJBQVMsRUFBQyw0Q0FBakI7QUFBOEQseUJBQUksVUFBbEU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBREEsZUFJQTtBQUFPLDJCQUFTLEVBQUMsNEhBQWpCO0FBQThJLG9CQUFFLEVBQUMsT0FBako7QUFBeUosc0JBQUksRUFBQztBQUE5SixtQkFBeUtGLFFBQVEsQ0FBQyxPQUFELENBQWpMO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURKLGVBT0k7QUFBSyx5QkFBUyxFQUFDLE1BQWY7QUFBQSx3Q0FDQTtBQUFPLDJCQUFTLEVBQUMsNENBQWpCO0FBQThELHlCQUFJLFVBQWxFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURBLGVBSUE7QUFBTywyQkFBUyxFQUFDLGlJQUFqQjtBQUFtSixvQkFBRSxFQUFDLFVBQXRKO0FBQWlLLHNCQUFJLEVBQUMsVUFBdEs7QUFBaUwsNkJBQVcsRUFBQztBQUE3TCxtQkFBb05BLFFBQVEsQ0FBQyxVQUFELENBQTVOO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSkEsZUFLQTtBQUFHLDJCQUFTLEVBQUMsZ0JBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQVBKLGVBY0k7QUFBSyx5QkFBUyxFQUFDLG1DQUFmO0FBQUEsd0NBQ0E7QUFBUSwyQkFBUyxFQUFDLDhHQUFsQjtBQUFpSSxzQkFBSSxFQUFDLFFBQXRJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQURBLGVBSUE7QUFBRywyQkFBUyxFQUFDLGlGQUFiO0FBQStGLHNCQUFJLEVBQUMsR0FBcEc7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQWRKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFESixlQXdCSTtBQUFHLHVCQUFTLEVBQUMsbUNBQWI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBeEJKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFYRixlQXVDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQXZDRixlQXdDRztBQUFBLG9DQUNLO0FBQUssdUJBQVMsRUFBQywyQkFBZjtBQUFBLHFDQUNFO0FBQUsseUJBQVMsRUFBQyxhQUFmO0FBQUEsdUNBQ0U7QUFBUSwyQkFBUyxFQUFDLG9GQUFsQjtBQUFBLDBDQUNFO0FBQUssNkJBQVMsRUFBQywrREFBZjtBQUErRSwyQkFBTyxFQUFDLFdBQXZGO0FBQW1HLHlCQUFLLEVBQUMsNEJBQXpHO0FBQUEsMkNBQ0U7QUFBTSx1QkFBQyxFQUFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBREYsZUFJRTtBQUFNLDZCQUFTLEVBQUMsOEJBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURMLGVBV0s7QUFBSyx1QkFBUyxFQUFDLHNCQUFmO0FBQUEscUNBQ0U7QUFBSyx5QkFBUyxFQUFDLGFBQWY7QUFBQSx1Q0FDRTtBQUFRLDJCQUFTLEVBQUMsa0ZBQWxCO0FBQUEsMENBQ0U7QUFBSyw2QkFBUyxFQUFDLCtEQUFmO0FBQStFLDJCQUFPLEVBQUMsV0FBdkY7QUFBbUcseUJBQUssRUFBQyw0QkFBekc7QUFBQSwyQ0FDRTtBQUFNLHVCQUFDLEVBQUM7QUFBUjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFERixlQUlFO0FBQU0sNkJBQVMsRUFBQyw2QkFBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBWEw7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQXhDSDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBaEJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQXNGRDs7R0F2R3VCRjtVQUdQTixvREFDbUJIOzs7TUFKWlMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvYXV0aC9sb2dpbi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCBBdmF0YXIgZnJvbSAnQG11aS9tYXRlcmlhbC9BdmF0YXInO1xyXG5pbXBvcnQgQnV0dG9uIGZyb20gJ0BtdWkvbWF0ZXJpYWwvQnV0dG9uJztcclxuaW1wb3J0IENzc0Jhc2VsaW5lIGZyb20gJ0BtdWkvbWF0ZXJpYWwvQ3NzQmFzZWxpbmUnO1xyXG5pbXBvcnQgVGV4dEZpZWxkIGZyb20gJ0BtdWkvbWF0ZXJpYWwvVGV4dEZpZWxkJztcclxuaW1wb3J0IEZvcm1Db250cm9sTGFiZWwgZnJvbSAnQG11aS9tYXRlcmlhbC9Gb3JtQ29udHJvbExhYmVsJztcclxuaW1wb3J0IENoZWNrYm94IGZyb20gJ0BtdWkvbWF0ZXJpYWwvQ2hlY2tib3gnO1xyXG5pbXBvcnQgTGluayBmcm9tICdAbXVpL21hdGVyaWFsL0xpbmsnO1xyXG5pbXBvcnQgUGFwZXIgZnJvbSAnQG11aS9tYXRlcmlhbC9QYXBlcic7XHJcbmltcG9ydCBCb3ggZnJvbSAnQG11aS9tYXRlcmlhbC9Cb3gnO1xyXG5pbXBvcnQgR3JpZCBmcm9tICdAbXVpL21hdGVyaWFsL0dyaWQnO1xyXG5pbXBvcnQgTG9ja091dGxpbmVkSWNvbiBmcm9tICdAbXVpL2ljb25zLW1hdGVyaWFsL0xvY2tPdXRsaW5lZCc7XHJcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ0BtdWkvbWF0ZXJpYWwvVHlwb2dyYXBoeSc7XHJcbmltcG9ydCB7IGNyZWF0ZVRoZW1lLCBUaGVtZVByb3ZpZGVyIH0gZnJvbSAnQG11aS9tYXRlcmlhbC9zdHlsZXMnO1xyXG5pbXBvcnQgTG9nb2RhbmFyZWtzYSBmcm9tICcuLi8uLi9pbWFnZXMvbG9nby1kYW5hcmVrc2EucG5nJ1xyXG5pbXBvcnQgSW1hZ2UgZnJvbSAnbmV4dC9pbWFnZSdcclxuaW1wb3J0IHt1c2VGb3JtfSBmcm9tIFwicmVhY3QtaG9vay1mb3JtXCJcclxuaW1wb3J0IHt1c2VTdGF0ZX0gZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQge2F1dGh9IGZyb20gJy4uLy4uL2xpYi9zZXJ2aWNlJ1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tICduZXh0L3JvdXRlcidcclxuXHJcblxyXG5cclxuZnVuY3Rpb24gQ29weXJpZ2h0KHByb3BzKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJib2R5MlwiIGNvbG9yPVwidGV4dC5zZWNvbmRhcnlcIiBhbGlnbj1cImNlbnRlclwiIHsuLi5wcm9wc30+XHJcbiAgICAgIHsnQ29weXJpZ2h0IMKpICd9XHJcbiAgICAgIDxMaW5rIGNvbG9yPVwiaW5oZXJpdFwiIGhyZWY9XCJodHRwczovL21hdGVyaWFsLXVpLmNvbS9cIj5cclxuICAgICAgICBZb3VyIFdlYnNpdGVcclxuICAgICAgPC9MaW5rPnsnICd9XHJcbiAgICAgIHtuZXcgRGF0ZSgpLmdldEZ1bGxZZWFyKCl9XHJcbiAgICAgIHsnLid9XHJcbiAgICA8L1R5cG9ncmFwaHk+XHJcbiAgKTtcclxufVxyXG5cclxuY29uc3QgdGhlbWUgPSBjcmVhdGVUaGVtZSgpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gU2lnbkluU2lkZSgpIHtcclxuXHJcblxyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpIFxyXG4gIGNvbnN0IHtyZWdpc3RlciwgaGFuZGxlU3VibWl0IH0gPSB1c2VGb3JtKCk7XHJcbiAgY29uc3Qgb25TdWJtaXQgPSBkYXRhID0+IHN1Ym1pdERhdGEoZGF0YSk7XHJcbiAgY29uc3Qgc3VibWl0RGF0YSA9IGFzeW5jIChkYXRhKSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGRhdGEsICdkYXRhcycpXHJcbiAgY29uc3QgYXV0aFJlc3BvbnNlID0gYXdhaXQgYXV0aC5sb2dpbihkYXRhLmVtYWlsLGRhdGEucGFzc3dvcmQpO1xyXG4gICAgY29uc29sZS5sb2coYXV0aFJlc3BvbnNlKVxyXG4gICAgICAgaWYgKCFhdXRoUmVzcG9uc2UuZXJyb3IpIHtcclxuICAgICAgcm91dGVyLnB1c2goJy9lZm9ybScpXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJvdXRlci5wdXNoKCcvbG9naW4nKVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxUaGVtZVByb3ZpZGVyIHRoZW1lPXt0aGVtZX0+XHJcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBjb21wb25lbnQ9XCJtYWluXCIgc3g9e3sgaGVpZ2h0OiAnMTAwdmgnIH19PlxyXG4gICAgICAgIDxDc3NCYXNlbGluZSAvPlxyXG4gICAgICAgIDxHcmlkXHJcbiAgICAgICAgICBpdGVtXHJcbiAgICAgICAgICB4cz17ZmFsc2V9XHJcbiAgICAgICAgICBzbT17NH1cclxuICAgICAgICAgIG1kPXs3fVxyXG4gICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgYmFja2dyb3VuZEltYWdlOiAndXJsKGh0dHBzOi8vc291cmNlLnVuc3BsYXNoLmNvbS9yYW5kb20pJyxcclxuICAgICAgICAgICAgYmFja2dyb3VuZFJlcGVhdDogJ25vLXJlcGVhdCcsXHJcbiAgICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogKHQpID0+XHJcbiAgICAgICAgICAgICAgdC5wYWxldHRlLm1vZGUgPT09ICdsaWdodCcgPyB0LnBhbGV0dGUuZ3JleVs1MF0gOiB0LnBhbGV0dGUuZ3JleVs5MDBdLFxyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kU2l6ZTogJ2NvdmVyJyxcclxuICAgICAgICAgICAgYmFja2dyb3VuZFBvc2l0aW9uOiAnY2VudGVyJyxcclxuICAgICAgICAgIH19XHJcbiAgICAgICAgLz5cclxuICAgICAgICA8R3JpZCBpdGVtIHhzPXsxNn0gc209ezh9IG1kPXs1fSBjb21wb25lbnQ9e1BhcGVyfSBlbGV2YXRpb249ezZ9IHNxdWFyZT5cclxuICAgICAgICAgIDxCb3hcclxuICAgICAgICAgICAgc3g9e3tcclxuICAgICAgICAgICAgICBteTogMTAsXHJcbiAgICAgICAgICAgICAgbXg6IDEwLFxyXG4gICAgICAgICAgICAgIGRpc3BsYXk6ICdmbGV4JyxcclxuICAgICAgICAgICAgICBmbGV4RGlyZWN0aW9uOiAnY29sdW1uJyxcclxuICAgICAgICAgICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPEltYWdlIHNyYz17TG9nb2RhbmFyZWtzYX0gLz5cclxuICAgICAgICAgICAgPGJyIC8+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIG1heC13LXhzXCI+XHJcbiAgICAgICAgICAgICAgICA8Zm9ybSBvblN1Ym1pdD17aGFuZGxlU3VibWl0KG9uU3VibWl0KX0gY2xhc3NOYW1lPVwiYmctZ3JheS0yMDAgc2hhZG93LW1kIHJvdW5kZWQgcHgtOCBwdC02IHBiLTggbWItNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWItNFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJibG9jayB0ZXh0LWdyYXktNzAwIHRleHQtc20gZm9udC1ib2xkIG1iLTJcIiBmb3I9XCJ1c2VybmFtZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBFbWFpbFxyXG4gICAgICAgICAgICAgICAgICAgIDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGlucHV0IGNsYXNzTmFtZT1cInNoYWRvdyBhcHBlYXJhbmNlLW5vbmUgYm9yZGVyIHJvdW5kZWQgdy1mdWxsIHB5LTIgcHgtMyB0ZXh0LWdyYXktNzAwIGxlYWRpbmctdGlnaHQgZm9jdXM6b3V0bGluZS1ub25lIGZvY3VzOnNoYWRvdy1vdXRsaW5lXCIgaWQ9XCJlbWFpbFwiIHR5cGU9XCJ0ZXh0XCIgey4uLnJlZ2lzdGVyKFwiZW1haWxcIil9Lz5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1iLTZcIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwiYmxvY2sgdGV4dC1ncmF5LTcwMCB0ZXh0LXNtIGZvbnQtYm9sZCBtYi0yXCIgZm9yPVwicGFzc3dvcmRcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgUGFzc3dvcmRcclxuICAgICAgICAgICAgICAgICAgICA8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpbnB1dCBjbGFzc05hbWU9XCJzaGFkb3cgYXBwZWFyYW5jZS1ub25lIGJvcmRlciByb3VuZGVkIHctZnVsbCBweS0yIHB4LTMgdGV4dC1ncmF5LTcwMCBtYi0zIGxlYWRpbmctdGlnaHQgZm9jdXM6b3V0bGluZS1ub25lIGZvY3VzOnNoYWRvdy1vdXRsaW5lXCIgaWQ9XCJwYXNzd29yZFwiIHR5cGU9XCJwYXNzd29yZFwiIHBsYWNlaG9sZGVyPVwiTWFzdWthbiBQYXNzd29yZFwiIHsuLi5yZWdpc3RlcihcInBhc3N3b3JkXCIpfSAvPlxyXG4gICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cInRleHQteHMgaXRhbGljXCI+UGxlYXNlIGNob29zZSBhIHBhc3N3b3JkLjwvcD5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXggaXRlbXMtY2VudGVyIGp1c3RpZnktYmV0d2VlblwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiYmctYmx1ZS01MDAgaG92ZXI6YmctYmx1ZS03MDAgdGV4dC13aGl0ZSBmb250LWJvbGQgcHktMiBweC00IHJvdW5kZWQgZm9jdXM6b3V0bGluZS1ub25lIGZvY3VzOnNoYWRvdy1vdXRsaW5lXCIgdHlwZT1cInN1Ym1pdFwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBNQVNVS1xyXG4gICAgICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImlubGluZS1ibG9jayBhbGlnbi1iYXNlbGluZSBmb250LWJvbGQgdGV4dC1zbSB0ZXh0LWJsdWUtNTAwIGhvdmVyOnRleHQtYmx1ZS04MDBcIiBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBCZWx1bSBNZW1pbGlraSBBa3VuID9cclxuICAgICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJ0ZXh0LWNlbnRlciB0ZXh0LWdyYXktNTAwIHRleHQteHNcIj5cclxuICAgICAgICAgICAgICAgICAgICAmY29weTsyMDIwIEFjbWUgQ29ycC4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cclxuICAgICAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDxiciAvPlxyXG4gICAgICAgICAgICAgPGZvcm0+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LXdyYXAgLW14LTMgbWItM1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy1mdWxsIHB4LTNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiYnRuIHB4LTAgdGV4dC13aGl0ZSBiZy1ncmF5LTkwMCBob3ZlcjpiZy1ncmF5LTgwMCB3LWZ1bGwgcm91bmRlZCBmbGV4IGl0ZW1zLWNlbnRlclwiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3ZnIGNsYXNzTmFtZT1cInctNCBoLTQgZmlsbC1jdXJyZW50IHRleHQtd2hpdGUgb3BhY2l0eS03NSBmbGV4LXNocmluay0wIG14LTRcIiB2aWV3Qm94PVwiMCAwIDE2IDE2XCIgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIDxwYXRoIGQ9XCJNNy45NSAwQzMuNTc4IDAgMCAzLjU3OCAwIDcuOTVjMCAzLjQ3OSAyLjI4NiA2LjQ2IDUuNDY2IDcuNTUzLjM5Ny4xLjQ5Ny0uMTk5LjQ5Ny0uMzk3di0xLjM5MmMtMi4xODcuNDk3LTIuNjgzLS45OTMtMi42ODMtLjk5My0uMzk4LS44OTUtLjg5NS0xLjE5My0uODk1LTEuMTkzLS42OTYtLjQ5Ny4xLS40OTcuMS0uNDk3Ljc5NS4xIDEuMTkyLjc5NSAxLjE5Mi43OTUuNjk2IDEuMjkyIDEuODg4Ljg5NSAyLjI4Ni42OTYuMS0uNDk3LjI5OC0uODk1LjQ5Ny0xLjA5My0xLjc5LS4yLTMuNTc4LS44OTUtMy41NzgtMy45NzUgMC0uODk1LjI5OC0xLjU5Ljc5NS0yLjA4Ny0uMS0uMi0uMzk3LS45OTQuMS0yLjA4NyAwIDAgLjY5NS0uMiAyLjE4Ni43OTVhNi40MDggNi40MDggMCAwMTEuOTg3LS4yOTljLjY5NiAwIDEuMzkyLjEgMS45ODguMjk5IDEuNDktLjk5NCAyLjE4Ni0uNzk1IDIuMTg2LS43OTUuMzk4IDEuMDkzLjE5OSAxLjg4OC4xIDIuMDg3LjQ5Ni41OTYuNzk1IDEuMjkxLjc5NSAyLjA4NyAwIDMuMDgtMS44ODkgMy42NzctMy42NzcgMy44NzUuMjk4LjM5OC41OTYuODk1LjU5NiAxLjU5djIuMTg3YzAgLjE5OC4xLjQ5Ny41OTYuMzk3QzEzLjcxNCAxNC40MSAxNiAxMS40MyAxNiA3Ljk1IDE1LjkgMy41NzggMTIuMzIzIDAgNy45NSAwelwiIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvc3ZnPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJmbGV4LWF1dG8gcGwtMjAgcHItMTAgLW1sLTIwXCI+Q29udGludWUgd2l0aCBHaXRIdWI8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LXdyYXAgLW14LTNcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInctZnVsbCBweC0zXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImJ0biBweC0wIHRleHQtd2hpdGUgYmctcmVkLTYwMCBob3ZlcjpiZy1yZWQtNzAwIHctZnVsbCByb3VuZGVkIGZsZXggaXRlbXMtY2VudGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzdmcgY2xhc3NOYW1lPVwidy00IGgtNCBmaWxsLWN1cnJlbnQgdGV4dC13aGl0ZSBvcGFjaXR5LTc1IGZsZXgtc2hyaW5rLTAgbXgtNFwiIHZpZXdCb3g9XCIwIDAgMTYgMTZcIiB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgPHBhdGggZD1cIk03LjkgN3YyLjRIMTJjLS4yIDEtMS4yIDMtNCAzLTIuNCAwLTQuMy0yLTQuMy00LjQgMC0yLjQgMi00LjQgNC4zLTQuNCAxLjQgMCAyLjMuNiAyLjggMS4xbDEuOS0xLjhDMTEuNSAxLjcgOS45IDEgOCAxIDQuMSAxIDEgNC4xIDEgOHMzLjEgNyA3IDdjNCAwIDYuNy0yLjggNi43LTYuOCAwLS41IDAtLjgtLjEtMS4ySDcuOXpcIiAvPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L3N2Zz5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiZmxleC1hdXRvIHBsLTE2IHByLTggLW1sLTE2XCI+Q29udGludWUgd2l0aCBHb29nbGU8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Zvcm0+XHJcbiAgICAgICAgICAgIDwvQm94PlxyXG4gICAgICAgIDwvR3JpZD5cclxuICAgICAgPC9HcmlkPlxyXG4gICAgPC9UaGVtZVByb3ZpZGVyPlxyXG4gICk7XHJcbn0iXSwibmFtZXMiOlsiUmVhY3QiLCJBdmF0YXIiLCJCdXR0b24iLCJDc3NCYXNlbGluZSIsIlRleHRGaWVsZCIsIkZvcm1Db250cm9sTGFiZWwiLCJDaGVja2JveCIsIkxpbmsiLCJQYXBlciIsIkJveCIsIkdyaWQiLCJMb2NrT3V0bGluZWRJY29uIiwiVHlwb2dyYXBoeSIsImNyZWF0ZVRoZW1lIiwiVGhlbWVQcm92aWRlciIsIkxvZ29kYW5hcmVrc2EiLCJJbWFnZSIsInVzZUZvcm0iLCJ1c2VTdGF0ZSIsImF1dGgiLCJ1c2VSb3V0ZXIiLCJDb3B5cmlnaHQiLCJwcm9wcyIsIkRhdGUiLCJnZXRGdWxsWWVhciIsInRoZW1lIiwiU2lnbkluU2lkZSIsInJvdXRlciIsInJlZ2lzdGVyIiwiaGFuZGxlU3VibWl0Iiwib25TdWJtaXQiLCJkYXRhIiwic3VibWl0RGF0YSIsImNvbnNvbGUiLCJsb2ciLCJsb2dpbiIsImVtYWlsIiwicGFzc3dvcmQiLCJhdXRoUmVzcG9uc2UiLCJlcnJvciIsInB1c2giLCJoZWlnaHQiLCJiYWNrZ3JvdW5kSW1hZ2UiLCJiYWNrZ3JvdW5kUmVwZWF0IiwiYmFja2dyb3VuZENvbG9yIiwidCIsInBhbGV0dGUiLCJtb2RlIiwiZ3JleSIsImJhY2tncm91bmRTaXplIiwiYmFja2dyb3VuZFBvc2l0aW9uIiwibXkiLCJteCIsImRpc3BsYXkiLCJmbGV4RGlyZWN0aW9uIiwiYWxpZ25JdGVtcyJdLCJzb3VyY2VSb290IjoiIn0=