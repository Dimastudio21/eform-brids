export const auth = {
  localLogin: async (username, password) => {
    const data = {
      username: username,
      password: password,
    }

    const res = await fetch(`/api/login`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    }).catch(e => {
      return {list: []}
    })

    if (res.status === 200) {
      return await res.json()
    } else {
      return {
        error: true,
        status: res.status,
      }
    }
  },

  login: async (username, password) => {
    var querystring = require('querystring');

    const data = {
      grant_type: "password",
      username: username,
      password: password,
      // "g-recaptcha-response": recaptchaToken
    }
    const res = await fetch(`https://apiauth-dev.moservice.id/oauth/token?action=login`, {
      method: 'POST',
      body: querystring.stringify(data),
      headers: {
        'Authorization': 'Basic d2ViX2FwcDptMHMzcnYxYzM=',
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    }).catch(e => {
      return {list: []}
    })
    if (res.status === 200) {
      const result = await res.json()
      const user = result

      return {
        id: user.user_id,// ini user_id atau cred_id ?
        credential: {
          token: user.access_token
        }
      }
    } else {
      return {
        error: true,
        status: res.status,
      }
    }
  },
}

export const user = {
  get: async (userId, token) => {
    const res = await fetch(`https://apiusers-dev.moservice.id/api/users/${userId}`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    }).catch(e => { return { list: [] } })

    if(res.status === 200){
      const result = await res.json()
      const user = result.data
      return {
        // ...userInitialState,
        id: user.id,
        firstname: user.firstName,
        lastname: user.lastName,
        image: {
          url: user.imageUrl,
          initial: String.getInitials(user.firstName + user.lastName),
        },
        credential: {
          id: user.credential.id,
          email: user.credential.email,
          token: token,
          provider: user.provider,
          mobileNo: user.credential.mobileNo
        }
      }
    }else {
      return {
        error: true,
        status: res.status,
      }
    }
  },
}